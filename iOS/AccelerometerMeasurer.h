
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SAMPLING_RATE 200
#define TIME_TO_CALIBRATE 0.5f // how long should we spend calibrating
#define NUM_MOVING_AVG_SAMPLES SAMPLING_RATE/10 // how many samples to use in the moving average
#define NUM_SAMPLES_TO_STOP SAMPLING_RATE/10    // how many samples of zero acceleration cause the velocity to be set to zero

@interface AccelerometerMeasurer : NSObject
{
	UIAccelerationValue a[3];
  double v[3]; // velocities
  double rate; // # of updates per second
  double lastUpdateTime;
  double numZeroAccel[3];
  
	double calibratedA[3];
  int countCalibrationSamples;
}

- (id)initWithSampleRate:(double)_rate;
- (void)startCalibration;
- (void)addAcceleration:(UIAcceleration*)accel;
- (double)a:(int)dim;
- (double)v:(int)dim;

@end

