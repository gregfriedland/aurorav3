// TODO
// -Use colors differently? see video of blue pattern with flashing red dots
// -don't go negative in color speed
// -add tabs for: program, settings, motion, sound, touch
// -disable settings not available in different programs
// -Text entry
// -add more colors and remove bad colors

#import "TableViewController.h"
#import "TouchViewController.h"
#import <Foundation/NSObjCRuntime.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CAAnimation.h>
#import <Foundation/Foundation.h>

#if USE_BT
typedef enum {
  IDLE = 0,
  SCANNING,
  CONNECTED,
} ConnectionState;
#endif

double constrain(double x, double min, double max) {
  return MIN(MAX(x, min), max);
}

double map(double x, double in_min, double in_max, double out_min, double out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


@interface TableViewController ()

#if USE_BT
@property CBCentralManager *cm;
@property ConnectionState state;
@property UARTPeripheral *currentPeripheral;
#endif

@end


@implementation TableViewController

#if USE_BT
@synthesize cm = _cm;
@synthesize currentPeripheral = _currentPeripheral;
#endif
@synthesize motionManager;



- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
      // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];

  // remove grid lines in grouped table display
  [self.tableView setSeparatorColor:[UIColor clearColor]];
  
#if USE_BT
  self.cm = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
  [self disableWidgets];
  btnConnect.backgroundColor = [UIColor colorWithRed:90/255.0 green:158/255.0 blue:148/255.0 alpha:1.0];
#endif
#if USE_SERIAL
  [self enableWidgets];
  btnConnect.backgroundColor = [UIColor colorWithRed:0/255.0 green:35/255.0 blue:68/255.0 alpha:1.0];
  btnConnect.enabled = false;
#endif

  [btnConnect setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  btnConnect.layer.borderColor = [UIColor blackColor].CGColor;
  btnConnect.layer.borderWidth = 1.0f;
  btnConnect.layer.cornerRadius = 8.0f;
  
  lastSendTimes = [NSMutableDictionary dictionary];
  for (int i=0; i<MAX_CMD; i++)
    [lastSendTimes setObject:[NSNumber numberWithDouble:0.0] forKey:[NSNumber numberWithInt:i]];

  accelMeas = [[AccelerometerMeasurer alloc] initWithSampleRate:SAMPLING_RATE];
  motionManager = [[CMMotionManager alloc] init];
  
  textEntry.delegate = self;
  
#if USE_SERIAL
  rscMgr = [[RscMgr alloc] init];
  [rscMgr setDelegate:self];
#endif
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)canBecomeFirstResponder {
  return YES;
}

-(void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
  [self resignFirstResponder];
  [super viewWillDisappear:animated];
}

#pragma mark - UI


- (void)highlightBtn:(UIButton*)btn
{
  bzrBtn.highlighted = NO;
  alienBlobBtn.highlighted = NO;
  paintBtn.highlighted = NO;
  tickerBtn.highlighted = NO;
  offBtn.highlighted = NO;
  phageBtn.highlighted = NO;
  btn.highlighted = YES;
}

- (void)disableWidgets
{
  for (UIButton *btn in @[ bzrBtn, paintBtn, alienBlobBtn, randomizeParamsBtn, touchBtn,
       tickerBtn, offBtn, phageBtn, textEntryDoneBtn]) {
    btn.enabled = NO;
    btn.alpha = 0.4;
  }
  
  [textEntry setUserInteractionEnabled:NO];
  textEntry.alpha = 0.4;
  paletteSlider.enabled = NO;
  speedSlider.enabled = NO;
  colorCyclingSlider.enabled = NO;
  zoomSlider.enabled = NO;
}

- (void)enableWidgets
{
  for (UIButton *btn in @[ bzrBtn, paintBtn, alienBlobBtn, randomizeParamsBtn, touchBtn,
       tickerBtn, offBtn, phageBtn]) {
    btn.enabled = YES;
    btn.alpha = 1.0;
  }
  
  [textEntry setUserInteractionEnabled:YES];
  textEntry.alpha = 1.0;
  paletteSlider.enabled = YES;
  speedSlider.enabled = YES;
  colorCyclingSlider.enabled = YES;
  zoomSlider.enabled = YES;
}


#if USE_BT
#pragma mark - BLE

- (void)bleBeginScan {
  if (self.currentPeripheral.peripheral.UUID != NULL) {
    NSLog(@"Found previous UUID: %@", self.currentPeripheral.peripheral.UUID);
    [self.cm retrievePeripherals:[NSArray arrayWithObject:(id)self.currentPeripheral.peripheral.UUID]];
  } else {
    self.state = SCANNING;
    NSLog(@"Started scan ...");
    [indConnecting startAnimating];
    
    [self.cm scanForPeripheralsWithServices:@[[UARTPeripheral serviceUUID]] options:@{CBCentralManagerScanOptionAllowDuplicatesKey: [NSNumber numberWithBool:NO]}];
  }
}


- (void)bleDidConnect:(CBPeripheral*) peripheral {
  NSLog(@"Did connect peripheral %@", peripheral.name);
  
  self.state = CONNECTED;  
  [indConnecting stopAnimating];
  
  btnConnect.backgroundColor = [UIColor colorWithRed:0/255.0 green:35/255.0 blue:68/255.0 alpha:1.0];
  [self enableWidgets];
  
  // disable connect button now
  btnConnect.enabled = NO;
  
  int paletteIndex = 0; //arc4random() % NUM_PALETTES;
  paletteSlider.value = (float)paletteIndex / (NUM_PALETTES-1);
  
  [self.currentPeripheral didConnect];
}


- (void)bleDidDiscover:(CBPeripheral*) peripheral {
  NSLog(@"Did discover peripheral %@", peripheral.name);
  [self.cm stopScan];
  
  self.currentPeripheral = [[UARTPeripheral alloc] initWithPeripheral:peripheral delegate:self];
  
  [self.cm connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: [NSNumber numberWithBool:YES]}];
}


- (void) bleStopScan {
  NSLog(@"Stopped scan");
  self.state = IDLE;
  [self.cm stopScan];
  [indConnecting stopAnimating];
}


- (void) bleDisconnect:(CBPeripheral*) peripheral {
  NSLog(@"Did disconnect peripheral %@", peripheral.name);
  
  self.state = IDLE;
  [self disableWidgets];
  btnConnect.backgroundColor = [UIColor colorWithRed:90/255.0 green:158/255.0 blue:148/255.0 alpha:1.0];
  btnConnect.enabled = YES; // reenable connect button
  
  [self.cm cancelPeripheralConnection:self.currentPeripheral.peripheral];
  [self.currentPeripheral didDisconnect];
}



#pragma mark - CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
  CBPeripheral *peripheral = [peripherals objectAtIndex:0];
  NSLog(@"Reconnecting to existing peripheral with UUID: %@", peripheral.UUID);
  [self bleDidConnect:peripheral];
  [self.cm connectPeripheral:peripheral options:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey: [NSNumber numberWithBool:YES]}];
}


- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
  if (central.state == CBCentralManagerStatePoweredOn)
  {
    NSLog(@"State -> powered on");
  }
}


- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
  [self bleDidDiscover:peripheral];
}


- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
  if ([self.currentPeripheral.peripheral isEqual:peripheral])
  {
    [self bleDidConnect:peripheral];
  }
}


- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
  if ([self.currentPeripheral.peripheral isEqual:peripheral])
  {
    [self bleDisconnect:peripheral];
  }
}


// When data is coming, this will be called
- (void) didReceiveData:(NSData *)dataObj
{
  [self processCmds:dataObj];
}
#endif


-(void) processCmds:(NSData*) dataObj
{
  const uint8_t *data = [dataObj bytes];

  NSLog(@"Received: %d bytes", [dataObj length]);

  // parse data, all commands are 3-bytes
  for (int i = 0; i < [dataObj length]; i+=3) {
    if (data[i+1] == 255) {
      switch(data[i]) {
        case CMD_TEXT_RECEIVED: {
          NSLog(@"Received ack for sent text.");
          sentTextReceived = true;
        }
      }
    }
    
    if (data[i+2] == 255) {
      float val;
      switch(data[i]) {
        case CMD_SET_SPEED: {
            speedSlider.value = (float) data[i+1] / MAX_ARG;
            break;
        }
        case CMD_SET_COLORCYCLING: {
            colorCyclingSlider.value = (float) data[i+1] / MAX_ARG;
            break;
        }
        case CMD_SET_ZOOM: {
            zoomSlider.value = (float) data[i+1] / MAX_ARG;
            break;
        }
        case CMD_SET_PALETTE: {
            paletteSlider.value = val = ((float) data[i+1]) / MAX_ARG;
            [self updatePaletteLabel];
            break;
        }
        case CMD_SET_PROGRAM: {
            if (data[i+1] == DRAWER_BZR) {
                [self performSelector:@selector(highlightBtn:) withObject:bzrBtn afterDelay:0.0];
            } else if (data[i+1] == DRAWER_ALIENBLOB) {
                [self performSelector:@selector(highlightBtn:) withObject:alienBlobBtn afterDelay:0.0];
            } else if (data[i+1] == DRAWER_PAINT) {
              [self performSelector:@selector(highlightBtn:) withObject:paintBtn afterDelay:0.0];
            } else if (data[i+1] == DRAWER_TEXT) {
              [self performSelector:@selector(highlightBtn:) withObject:tickerBtn afterDelay:0.0];
            } else if (data[i+1] == DRAWER_OFF) {
              [self performSelector:@selector(highlightBtn:) withObject:offBtn afterDelay:0.0];
            } else if (data[i+1] == DRAWER_PHAGE) {
              [self performSelector:@selector(highlightBtn:) withObject:phageBtn afterDelay:0.0];
            }
            break;
        }
      }
    }
  }
}

#pragma mark - Actions

// Connect button will call to this
- (IBAction)connect:(id)sender
{
#if USE_BT
  switch (self.state) {
  case IDLE:
    [self bleBeginScan];
    break;
    
  case SCANNING:
    [self bleStopScan];
    break;
    
  case CONNECTED:
    [self bleDisconnect:self.currentPeripheral.peripheral];
    break;
  }
#endif
}

-(IBAction)sendProgram:(id)sender
{
  int programNum;
  if (sender == bzrBtn) {
    programNum = DRAWER_BZR;
  } else if (sender == alienBlobBtn) {
    programNum = DRAWER_ALIENBLOB;
  } else if (sender == paintBtn) {
    programNum = DRAWER_PAINT;
  } else if (sender == tickerBtn) {
    programNum = DRAWER_TEXT;
  } else if (sender == offBtn) {
    programNum = DRAWER_OFF;
  } else if (sender == phageBtn) {
    programNum = DRAWER_PHAGE;
  } else {
    return;
  }
  
  UInt8 buf[3] = {CMD_SET_PROGRAM, programNum, 0xFF};
  [self sendData:buf ofSize:3];
  
  // send delayed message to keep the desired button highlighted
  [self performSelector:@selector(highlightBtn:) withObject:sender afterDelay:0.0];

}

// IBAction for changing the palette slider value
-(IBAction)palleteSliderChanged:(UISlider *)sender
{
  [self updatePaletteLabel];
}

// IBAction for the end of a palette slider change
-(IBAction)paletteSliderDone:(UISlider *)sender
{
  [self sendSetPalette:sender.value];
}

// IBAction for changing the speed slider value
-(IBAction)speedSliderChanged:(UISlider *)sender
{
  int value = (int) (sender.value * MAX_ARG);
  UInt8 buf[3] = {CMD_SET_SPEED, value, 0xFF};
  [self sendData:buf ofSize:3];
}

// IBAction for changing the color cycling slider value
-(IBAction)colorCyclingSliderChanged:(UISlider *)sender
{
  int value = (int) (sender.value * MAX_ARG);
  UInt8 buf[3] = {CMD_SET_COLORCYCLING, value, 0xFF};
  [self sendData:buf ofSize:3];
}

// IBAction for changing the zoom slider value
-(IBAction)zoomSliderChanged:(UISlider *)sender
{
  int value = (int) (sender.value * MAX_ARG);
  UInt8 buf[3] = {CMD_SET_ZOOM, value, 0xFF};
  [self sendData:buf ofSize:3];
}

// IBAction for randomizing parameters and restarting the program
-(IBAction)randomizeParams:(id)sender
{
  float paletteVal = (arc4random() % NUM_PALETTES) / (float)(NUM_PALETTES-1);
  [self sendSetPalette:paletteVal];
  
  int speed = arc4random() % MAX_ARG;
  speedSlider.value = (float)speed / MAX_ARG;
  UInt8 buf[3] = {CMD_SET_SPEED, speed, 0xFF};
  [self sendData:buf ofSize:3];

  int colorCycling = arc4random() % MAX_ARG;
  colorCyclingSlider.value = (float)colorCycling / MAX_ARG;
  UInt8 buf2[3] = {CMD_SET_COLORCYCLING, colorCycling, 0xFF};
  [self sendData:buf2 ofSize:3];        

  int zoom = arc4random() % MAX_ARG;
  zoomSlider.value = (float)zoom / MAX_ARG;
  UInt8 buf3[3] = {CMD_SET_ZOOM, zoom, 0xFF};
  [self sendData:buf3 ofSize:3];

  [self sendReset];
//  if (bzrBtn.highlighted) [self sendProgram:bzrBtn];
//  else if (alienBlobBtn.highlighted) [self sendProgram:alienBlobBtn];
//  else if (paintBtn.highlighted) [self send:paintBtn];
}

- (IBAction) motionBtnDown:(UIButton*)btn
{
  [self startMotionDetect];
}

- (IBAction) motionBtnUp:(UIButton*)btn
{
  [self stopMotionDetect];
}

- (IBAction) touchBtn:(UIButton*)btn
{
  TouchViewController *controller = [[TouchViewController alloc] init];
  TouchView *touchView = [[TouchView alloc] init]; //]WithFrame:CGRectMake(10, 10, screenBounds.size.width - 100, screenBounds.size.height-100)];
  touchView.backgroundColor = [UIColor colorWithRed:90/255.0 green:158/255.0 blue:148/255.0 alpha:1.0];
  controller.view = touchView;
  
  // create the nav controller and add the done/reset buttons to the bar
  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
  controller.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:controller action:@selector(done:)];
  controller.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc ] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:controller action:@selector(reset:)];

  [self presentViewController:navigationController animated:YES completion:nil];

}

- (IBAction) textEntryDone:(UIButton*)btn
{
  [textEntry resignFirstResponder];
  textEntryDoneBtn.enabled = false;
  textEntryDoneBtn.alpha = 0.4;
  
  char buf[MAX_TEXT_LENGTH];
  [textEntry.text getCString:buf maxLength:MAX_TEXT_LENGTH encoding:NSASCIIStringEncoding];
  [self sendText:buf];
}


#pragma mark - Commands

// Send a cmd
-(void)sendData:(UInt8*)buf ofSize:(int)nBytes
{
  [self sendData:buf ofSize:nBytes withThrottling:true];
}

-(void)sendData:(UInt8*)buf ofSize:(int)nBytes withThrottling:(bool)throttle
{
  NSNumber *cmd = [NSNumber numberWithInt:buf[0]];
  double currTime = CACurrentMediaTime();
  double lastSendTime = [[lastSendTimes objectForKey:cmd] doubleValue];
  if (currTime >= lastSendTime + MIN_SEND_WAIT || !throttle) {
    for (int i=0; i<nBytes; i++) {
      NSLog(@"TX: %d", buf[i]);
    }
    
#if USE_BT
    NSData *data = [[NSData alloc] initWithBytes:buf length:nBytes];
    [self.currentPeripheral writeRawData:data];
#endif
#if USE_SERIAL
    [rscMgr write:buf Length:nBytes];
#endif
    [lastSendTimes setObject:[NSNumber numberWithDouble:currTime] forKey:cmd];
    NSLog(@"Done sending data");
  }
}

// Send a set palette cmd and update the label
-(void)sendSetPalette:(float)paletteValue
{
  paletteSlider.value = paletteValue;
  [self updatePaletteLabel];

  int sendValue = paletteValue * MAX_ARG;
  UInt8 buf[3] = {CMD_SET_PALETTE, sendValue, 0xFF};
  [self sendData:buf ofSize:3];
}

- (void) updatePaletteLabel
{
  int txtVal = (int) (paletteSlider.value * (NUM_PALETTES-1)) + 1;
  
  NSString *baseString = @"Palette\n#";
  paletteLabel.text = [baseString stringByAppendingFormat: @"%d", txtVal];
}

// Send a set palette cmd
-(void)sendTouch:(int)touchNum x:(float)x y:(float)y
{
  UInt8 buf[5] = {CMD_SET_TOUCH, touchNum, (int)(x*MAX_ARG), (int)(y*MAX_ARG), 0xFF};
  [self sendData:buf ofSize:5];
}

-(void)sendReset
{
  UInt8 buf[2] = {CMD_RESET, 0xFF};
  [self sendData:buf ofSize:2];
}

-(void)sendText:(char *)txt
{
  UInt8 buf[2] = {CMD_CLEAR_TEXT, 0xFF};
  [self sendData:buf ofSize:2];
  [NSThread sleepForTimeInterval:0.05];

  sendingText = [[NSString alloc] initWithCString:txt encoding:NSASCIIStringEncoding];

  [NSTimer scheduledTimerWithTimeInterval:0.1
                                   target:self
                                 selector:@selector(sendTextAsync:)
                                 userInfo:nil
                                  repeats:NO];
  sentTextReceived = true;

  [indWaiting startAnimating];
}

-(void)sendTextAsync:(NSTimer*) timer
{  
  if (sentTextReceived) {
    int length = MIN(18, sendingText.length);
    //NSMutableData *data = [[NSMutableData alloc] initWithCapacity:length+2];
    UInt8 buf[20];
    buf[0] = CMD_APPEND_TEXT;
    [sendingText getCString:(char*)&buf[1] maxLength:length encoding:NSASCIIStringEncoding];
    [sendingText getBytes:buf+1 maxLength:length usedLength:NULL encoding:NSASCIIStringEncoding options:NULL range:NSMakeRange(0, length) remainingRange:NULL];
    buf[length+1] = 0xFF;

    [self sendData:buf ofSize:(length+2)];
    sendingText = [sendingText substringFromIndex:length];
    sentTextReceived = false;
  }

  if (sendingText.length != 0) {
    [NSTimer scheduledTimerWithTimeInterval:0.1
                                   target:self
                                   selector:@selector(sendTextAsync:)
                                   userInfo:nil
                                   repeats:NO];
  } else {
    [indWaiting stopAnimating];
  }
}


// Send a heartbeat as a result of a timer trigger
//-(void) sendHeartbeat:(NSTimer *)timer
//{
//    if (!touchView.isUserInteractionEnabled) return;
//    UInt8 buf[2] = {CMD_HEARTBEAT, 0xFF};
//    [self sendData:buf ofSize:2];
//}


# pragma mark - Motion

// recognize shake events
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
  if (motion == UIEventSubtypeMotionShake)
  {
    NSInteger randomNumber = arc4random() % NUM_PALETTES;
    UInt8 buf[3] = {CMD_SET_PALETTE, randomNumber, 0xFF};
    [self sendData:buf ofSize:3];
  }
}

- (void)stopMotionDetect
{
	[[UIAccelerometer sharedAccelerometer] setDelegate:nil];
}

- (void)startMotionDetect
{
  [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1.0 / SAMPLING_RATE];
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
  
  [accelMeas startCalibration];
}

// UIAccelerometerDelegate method, called when the device accelerates.
- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
  [accelMeas addAcceleration:acceleration];
  
  double v = constrain([accelMeas v:0], -MAX_VELOCITY, MAX_VELOCITY);
//  double vy = constrain(accelFilter.vy, -MAX_VELOCITY, MAX_VELOCITY);
//  double vz = constrain(accelFilter.vz, -MAX_VELOCITY, MAX_VELOCITY);
  if (fabs(v - lastV) > MIN_VELOCITY_CHANGE) {
    int val = round(map(v, -MAX_VELOCITY, MAX_VELOCITY, 0, MAX_ARG));
    UInt8 buf[3] = {CMD_SET_SPEED, val, 0xFF};
    [self sendData:buf ofSize:3];
    lastV = v;
  }
}

-(NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
  return YES;
}

- (IBAction)unwindToViewControllerNameHere:(UIStoryboardSegue *)segue {
  //nothing goes here
}

#pragma mark - UITextViewDelegate

// called when user enters text
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
  if ([textView.text length] + [text length] >= MAX_TEXT_LENGTH) {
    return NO;
  } else {
    return YES;
  }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
  if (textView == textEntry) {
    textEntryDoneBtn.enabled = true;
    textEntryDoneBtn.alpha = 1.0;
  }
}


#pragma mark - RscMgrDelegate methods
#if USE_SERIAL

- (void) cableConnected:(NSString *)protocol {
  NSLog(@"Serial cable connected");
  [rscMgr setBaud:SERIAL_BAUD];
	[rscMgr open];
}

- (void) cableDisconnected {
  
}

- (void) portStatusChanged {
  
}

- (void) readBytesAvailable:(UInt32)numBytes {
  NSLog(@"readBytesAvailable called on thread: %@", [NSThread currentThread]);
  UInt8 rxBuffer[MAX_TEXT_LENGTH];
  int bytesRead;
  
  // Read the data out
  bytesRead = [rscMgr read:rxBuffer length:numBytes];
  NSData *dataObj = [[NSData alloc] initWithBytes:rxBuffer length:bytesRead];

  [self processCmds:dataObj];
  
}

- (BOOL) rscMessageReceived:(UInt8 *)msg TotalLength:(int)len {
  return FALSE;
}

- (void) didReceivePortConfig {
}
#endif

@end
