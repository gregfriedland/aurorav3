

#import "TouchView.h"
#import "TableViewController.h"

@implementation TouchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(TableViewController*)getRootController
{
  return (TableViewController*) [[[UIApplication sharedApplication] keyWindow] rootViewController];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //TableViewController *controller = [self getController];
    //controller.tableView.scrollEnabled = NO;
    [self touchesHandler:touches withEvent:event];
    NSLog(@"Touch began");
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //TableViewController *controller = [self getRootController];
    //controller.tableView.scrollEnabled = YES;
    [self touchesHandler:touches withEvent:event];
    NSLog(@"Touch ended");
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesHandler:touches withEvent:event];
    //NSLog(@"Touch moved");
}

-(void)touchesHandler:(NSSet *)touches withEvent:(UIEvent *)event
{
    TableViewController *controller = [self getRootController];
    
    int width = self.bounds.size.width;
    int height = self.bounds.size.height;

    int i=0;
    NSEnumerator *enumerator = [touches objectEnumerator];
    id value;
    while ((value = [enumerator nextObject])) {
        UITouch* touch = (UITouch*) value;
        CGPoint location = [touch locationInView:touch.view];
        float x = ((float)location.x / width);
        float y = ((float)location.y / height);
        
        [controller sendTouch:i x:x y:y];
        i++;
        
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
