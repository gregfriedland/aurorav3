#define USE_SERIAL 1
#define USE_BT 0

#import <UIKit/UIKit.h>

#if USE_BT
#import "UARTPeripheral.h"
#endif

#if USE_SERIAL
#import "RscMgr.h"
#define SERIAL_BAUD 57600
#endif

#import "AccelerometerMeasurer.h"
#import <CoreMotion/CoreMotion.h>

@interface TableViewController : UITableViewController <
#if USE_BT
CBCentralManagerDelegate,UARTPeripheralDelegate,
#endif
#if USE_SERIAL
RscMgrDelegate,
#endif
UIAccelerometerDelegate, UITextViewDelegate>
{
  IBOutlet UIButton *btnConnect;
  IBOutlet UIButton *bzrBtn;
  IBOutlet UIButton *paintBtn;
  IBOutlet UIButton *alienBlobBtn;
  IBOutlet UIButton *tickerBtn;
  IBOutlet UIButton *offBtn;
  IBOutlet UIButton *phageBtn;
  IBOutlet UIButton *randomizeParamsBtn;
  
  IBOutlet UISlider *paletteSlider;
  IBOutlet UISlider *speedSlider;
  IBOutlet UISlider *colorCyclingSlider;
  IBOutlet UISlider *zoomSlider;

  IBOutlet UITextView *textEntry;
  IBOutlet UIButton *textEntryDoneBtn;
  IBOutlet UIButton *motionBtn;
  IBOutlet UIButton *touchBtn;
  IBOutlet UILabel *paletteLabel;
  IBOutlet UIActivityIndicatorView *indConnecting;
  NSMutableDictionary *lastSendTimes; // stores the last send time of each cmd so we don't overload the ble pipe

  AccelerometerMeasurer *accelMeas;
  CMMotionManager *motionManager;
  double lastV;
  bool sentTextReceived;
  NSString *sendingText;
  IBOutlet UIActivityIndicatorView *indWaiting;
  
#if USE_SERIAL
  RscMgr *rscMgr;
#endif
}

@property (readonly) CMMotionManager *motionManager;

-(void)sendTouch:(int)touchNum x:(float)x y:(float)y;
-(void)sendReset;

@end

#define MAX_TEXT_LENGTH 2048
#define MAX_TOUCHES 5
#define NUM_PALETTES 201
#define MAX_ARG 254
#define MIN_SEND_WAIT 0.05
#define MAX_VELOCITY 10.0f // what's the max velocity we use to map the data
#define MIN_VELOCITY_CHANGE MAX_VELOCITY/50 // what's the smallest change in velocity we will send an update for

enum DrawerType {
  DRAWER_BZR = 0,
  DRAWER_ALIENBLOB = 1,
  DRAWER_TEXT = 2,
  DRAWER_PAINT = 3,
  DRAWER_TEST = 4,
  DRAWER_PHAGE = 5,
  DRAWER_OFF = 6,
  DRAWER_EQUALIZER = 7,
  DRAWER_BEATBALLS = 8,
  MAX_USED_DRAWER = 9,
  // Put unused drawers down here
};




// Define the command types; the structure is 1-byte command, multi-byte payload, end with 255
enum Command {
  CMD_NONE         = 0x00, // noop; e.g. can be used to send BTLE 'credits' from iOS to the BTLE shield
  CMD_NEXT_PROGRAM = 0x01, // one arg (up/down)
  CMD_NEXT_PALETTE = 0x02, // one arg (up/down)
  CMD_CLEAR_TEXT   = 0x03, // text string in payload; text entry on ipad
  CMD_SET_SPEED    = 0x04, // one arg
  CMD_SET_COLORCYCLING    = 0x05, // one arg
  CMD_SET_ZOOM  = 0x06, // one arg
  CMD_SET_CUSTOM  = 0x07, // one arg
  CMD_RESET        = 0x08, // one arg (up/down)
  CMD_SET_BRIGHTNESS = 0x09, // one arg
  CMD_SET_MIN_SATURATION = 0x0A, // one arg
  CMD_SET_PROGRAM    = 0x0B, // one arg
  CMD_SET_PALETTE    = 0x0C, // one arg
  CMD_SET_POSITION    = 0x0D, // one arg
  CMD_SET_COLOR_INDEX    = 0x0E, // one arg
  CMD_SET_HUE_SHIFT    = 0x0F, // one arg
  
  CMD_ENABLE_AUDIO = 0x10, // two args
  CMD_SET_AUDIO_SENSITIVITY = 0x11, // two args
  CMD_APPEND_TEXT  = 0x12, // text string in payload; text entry on ipad
  CMD_TEXT_RECEIVED  = 0x13, // text string was received, ready to receive next one
  
  CMD_SET_TOUCH = 0x17, // three args
  CMD_HEARTBEAT = 0x1C, // no args
  MAX_CMD = 0x1D,  
};

