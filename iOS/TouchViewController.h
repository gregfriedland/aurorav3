//
//  TouchViewController.h
//  Aurora
//
//  Created by Greg Friedland on 8/2/13.
//  Copyright (c) 2013 RedBearLab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchView.h"

@interface TouchViewController : UITableViewController
{
  IBOutlet TouchView *touchView;
  IBOutlet UIButton *resetBtn;
  IBOutlet UIButton *doneBtn;
}

@end
