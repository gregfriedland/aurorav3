
double smoothVal(double prevVal, double newVal, int N) {
  return prevVal + (newVal - prevVal)/N;
}

#import "AccelerometerMeasurer.h"

@implementation AccelerometerMeasurer

- (id)initWithSampleRate:(double)_rate
{
	self = [super init];
	if(self != nil)
	{
    rate = _rate;
	}
	return self;
}

- (double)a:(int)dim {
  return a[dim];
}

- (double)v:(int)dim {
  return v[dim];
}

- (void)startCalibration
{
  countCalibrationSamples = 0;
  for (int i=0; i<3; i++) {
    a[i] = v[i] = calibratedA[i] = numZeroAccel[i] = 0;
  }
}

// calculate smoothed acceleration and velocity from the accelerometer
// when facing the screen, +x is to the left, +z is backwards, and +y is down
// http://www.freescale.com/files/sensors/doc/app_note/AN3397.pdf
- (void)addAcceleration:(UIAcceleration *)accel
{
  double measA[3] = {accel.x, accel.y, accel.z};
  
  int totalCalibrationSamples = (int) (rate * TIME_TO_CALIBRATE);
  if (countCalibrationSamples < totalCalibrationSamples) {
    for (int i=0; i<3; i++) {
      calibratedA[i] += measA[i];
    }
    countCalibrationSamples++;
  } else if (countCalibrationSamples == totalCalibrationSamples) {
    for (int i=0; i<3; i++) {
      calibratedA[i] /= countCalibrationSamples;
      numZeroAccel[i] = 0;
    }

    countCalibrationSamples++;
    lastUpdateTime = CACurrentMediaTime();
  } else {
    // perform an exponential moving average to smooth out noise
    double newA[3];
    NSString *s = @"Pre: ";
    for (int i=0; i<3; i++) {
      newA[i] = smoothVal(a[i], measA[i] - calibratedA[i], NUM_MOVING_AVG_SAMPLES);
    }

    // set the acceleration to zero if it is under a threshold
    for (int i=0; i<3; i++) {
      if (fabs(newA[i]) < 0.01) newA[i] = 0;
      
      s = [s stringByAppendingFormat:@"a%d = %5.2f ", i, newA[i]];
    }
    
    // look for no movement on the x axis indicating a stopped state
    if (newA[0] == 0) numZeroAccel[0]++;
    else numZeroAccel[0] = 0;

    double currentTime = CACurrentMediaTime();
    for (int i=0; i<3; i++) {
      if (numZeroAccel[0] >= NUM_SAMPLES_TO_STOP) {
        // set the velocity to zero if the acceleration has been zero for some time
        v[i] = 0;
        
        // TODO: restart calibration process here?
      } else {
        // integrate the acceleration with simple interpolation
        v[i] += newA[i] + (newA[i] - a[i]) / 2 * (currentTime - lastUpdateTime);
      }
      a[i] = newA[i];
      
      s = [s stringByAppendingFormat:@"v%d = %6.3f ", i, v[i]];
    }
    
    lastUpdateTime = currentTime;

    static int iter = 0;
    if (iter % 10 == 0) {
      NSLog(@"%@", s);
    }
    iter++;
  }
}


@end
