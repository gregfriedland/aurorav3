//
//  TouchViewController.m
//  Aurora
//
//  Created by Greg Friedland on 8/2/13.
//  Copyright (c) 2013 RedBearLab. All rights reserved.
//

#import "TouchViewController.h"
#import "TableViewController.h"


@interface TouchViewController ()

@end

@implementation TouchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskLandscape;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
  return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

-(BOOL)shouldAutorotate
{
  return YES;
}

-(IBAction) reset:(id)sender
{
  TableViewController *controller = (TableViewController*) [[[UIApplication sharedApplication] keyWindow] rootViewController];
  [controller sendReset];
}

-(IBAction) done:(id)sender
{
  // return screen rotation to normal:
  [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationPortrait;
  [self.presentingViewController dismissViewControllerAnimated: TRUE completion: nil];  
}
@end
