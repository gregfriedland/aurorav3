# convert an animated gif to a C-style array that can be used on Arduino
# Usage: animatedGifToArrayWithColorMap.py image.gif


from PIL import Image
import sys
import numpy

# convert RGBA tuple to hex string
def get_pixel_hex(pixel):
    col = pixel[0] * 256 * 256 + pixel[1] * 256 + pixel[2]
    hex_str = "0x%06X" % (col)
    return hex_str


# get list of hex strings used in this image
def get_color_map(im):
    cm = set()
    for pixel in im.getdata():
        cm.add(get_pixel_hex(pixel))
    return cm


# build body of array for this image using indices to this colormap
def build_image_body_array_str(pic, cm):
    pix = numpy.array(pic)

    body = ""
    for y in range(pix.shape[0]):
        for x in range(pix.shape[1]):
            hex_str = get_pixel_hex(pix[y,x,:])
            cm_index = cm.index(hex_str)
            body += "%3d, " % cm_index
        body += "\n"

    return body


# get images contained in this animated gif
def get_animated_images(anim_im):
    anim_im.seek(0)
    
    # Read all images inside
    ims = []
    try:
        while True:
            im = anim_im.convert() # Remove palette
            ims += [im]
            anim_im.seek(anim_im.tell()+1)
    except EOFError:
        pass
    
    return ims

def build_images_array_str(ims):
    # build colormap of all images
    cm = get_color_map(ims[0])
    for im in ims[1:]:
        cm = cm.union(get_color_map(im))
    cm = list(cm)

    cm_body = ", ".join(cm)
    body = "\n".join([build_image_body_array_str(im, cm) for im in ims])

    # print out header defining constants
    out = """
#define WIDTH %d
#define HEIGHT %d
#define NUM_FRAMES %d
#define NUM_COLORS %d

const int32_t colormap[] = 
{%s};

const int8_t img[] = 
{%s};
""" % (ims[0].size[0], ims[0].size[1], len(ims), len(cm), cm_body, body)
    
    return out


if __name__ == "__main__":
    anim_im = Image.open(open(sys.argv[1]))
    ims = get_animated_images(anim_im)
    print build_images_array_str(ims)

