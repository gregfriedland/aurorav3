/*
The Paint drawer: allow touches to appear on the display.

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef PAINTDRAWER_H
#define PAINTDRAWER_H

#include "Drawer.h"

#define PAINT_SPEED_MULTIPLIER 10

class PaintDrawer : public Drawer {
 public:
 PaintDrawer(int width, int height, DrawerSettings *settings, GlobalSettings *globalSettings) :
  Drawer(width, height, settings, globalSettings) {
    clear();
  }

  int getType() { return DRAWER_PAINT; }
  void setup() { clear(); }
  void reset() { clear(); }
  void draw(void (*insideDrawCallback)()) {
    Drawer::draw(insideDrawCallback);

    for (int i=0; i<MAX_TOUCHES; i++) {
      int x, y;
      if (settings->isTouching(i, &x, &y)) {
        // set the index in the canvas array but don't allow zero if we've touched
        canvas[x][y] = int(settings->pos + i * PALETTE_SIZE / MAX_TOUCHES) % (PALETTE_SIZE - 1) + 1;
      }
    }

    for (int x=0; x<width; x++) {
      for (int y=0; y<height; y++) {
        if (canvas[x][y] == 0) {
          setColor(x,y,0,0,0);
        } else {
          setColor(x,y,canvas[x][y]);
        }
      }
    }
  }

  void clear() {
    for (int x=0; x<width; x++) {
      for (int y=0; y<height; y++) {
        canvas[x][y] = 0;
      }
    }
  }
  
 private:
  int canvas[WIDTH][HEIGHT];
};

#endif
