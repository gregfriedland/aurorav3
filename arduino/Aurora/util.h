/*
Utility functions
*/

#ifndef UTIL_H
#define UTIL_H

#include "Config.h"
#include "RGB_HSV.h"
#include <stdint.h>
#include <Arduino.h>

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#ifdef ARDUINO_DUE
uint32_t trueRandom();
void startTimer(Tc *tc, uint32_t channel, IRQn_Type irq, uint32_t frequency);
#endif

float dist(float x1, float y1, float x2, float y2);

uint16_t interp(uint8_t start, uint8_t end, uint8_t index, uint8_t length);



// utilities for fixed point arithmetic
typedef int32_t FixedPoint;
typedef int64_t LongFixedPoint;
#define multfix(a,b,factor) ((FixedPoint)((((LongFixedPoint)(a))*((LongFixedPoint)(b)))>>factor)) //multiply two fixed #don't use this-too slow:
#define divfix(a,b,factor)  ((FixedPoint)((((LongFixedPoint)(a))<<factor)/((LongFixedPoint)(b)))) //divide two fixed #:don't use this-too slow:

#define int2fix(a,factor)   ((a)<<(factor))           //Convert char to fix. a is a char
#define fix2int(a,factor)   ((a)>>(factor))          //Convert fix to char. a is an int
#define float2fix(a,factor) ((FixedPoint)((a)*(1<<(factor))))         //Convert float to fix. a is a float
#define fix2float(a,factor) ((float)(a)/(1<<(factor)))         //Convert fix to float. a is an int

#endif
