//
//  Serial.cpp
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#include "Config.h"

#if USE_SERIAL

#include "Serial.h"
#include "util.h"

static BufferType rxBuf[SERIAL_BUFFER_LENGTH]; // array to hold incoming data
static int rxBufIndex;

void serial_setup() {
#if DEBUG_LEVEL >= 1
  Serial.println("Serial setup");
#endif
  Serial1.begin(SERIAL_BAUD);
}

// pop data from the receive buffer and copy it into the passed char array
// returns the length of the data received
int serial_pop_rx_data(BufferType *buffer, int len, int endChar) {
  if (rxBufIndex == 0) return 0;
#if DEBUG_LEVEL >= 3
  p("serial_pop_rx_data start: len "); p(len); p("\n");
#endif
  
  // find the last pos to pop
  int popLen = 0;
  if (endChar != -1) {
    while (popLen < rxBufIndex && rxBuf[popLen] != (BufferType) endChar)
      popLen++;
    popLen++; // include endChar
    
    // if the end char wasn't found then exit
    if (rxBuf[popLen-1] != (BufferType) endChar) return 0;
    popLen = MIN(MIN(popLen, rxBufIndex), len);
  } else {
    popLen = MIN(len, rxBufIndex);
  }
    
#if DEBUG_LEVEL >= 3
  p("serial_pop_rx_data mid\n");
  p("rxBufIndex="); p(rxBufIndex); p("; popLen="); p(popLen); p("\n");
#endif

  memcpy(buffer, rxBuf, popLen);
  BufferType tmp[SERIAL_BUFFER_LENGTH];
  memcpy(tmp, rxBuf+popLen, rxBufIndex-popLen);
  memcpy(rxBuf, tmp, rxBufIndex-popLen);
  rxBufIndex = rxBufIndex - popLen;

#if DEBUG_LEVEL >= 3
  p("serial_pop_rx_data done\n");
#endif
  return popLen;
}


void serial_loop() {
  while (Serial1.available()) {
    byte inByte = (byte)Serial1.read();
#if DEBUG_LEVEL >= 4
    p("Serial rx: "); Serial.print((int)inByte); p("\n");
#endif
    rxBuf[rxBufIndex++] = inByte;
    if (rxBufIndex >= SERIAL_BUFFER_LENGTH) rxBufIndex = SERIAL_BUFFER_LENGTH - 1;
  }
}

#endif // USE_SERIAL