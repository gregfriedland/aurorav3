//
//  Palette.h
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#ifndef Palette_h
#define Palette_h

#include <stdint.h>
#include "Config.h"

typedef struct {
  uint8_t r[PALETTE_SIZE];
  uint8_t g[PALETTE_SIZE];
  uint8_t b[PALETTE_SIZE];
  int index;
} Palette;


void loadPalette(int paletteIndex, void *palette);

#endif
