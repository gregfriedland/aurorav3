/*
Interface with WS2801 drivers using bit banging

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

// Timing:
//    Off drawer or update_LEDs: 250fps
//    Off drawer with upate_LEDs: 53fps
//    Off drawer with update_LEDs but no sendRGB: 250fps
//    Off drawer with upate_LEDs but no 20xnops (for loop): 170fps
//    BZR drawer with upate_LEDs but no 20xnops (for loop): 89fps
//    BZR drawer with upate_LEDs but no 5xnops: 82fps
//    BZR drawer with upate_LEDs but no 20xnops: 66fps
//    alienblob drawer with upate_LEDs but no 20xnops BT: 34fps
//    bzr drawer with upate_LEDs but no 20xnops BT Audio: 42fps (audio at 137/200 fps)
//    bzr drawer with upate_LEDs but no 20xnops BT Audio with no readSpectrum-delay: 39fps (audio at 80/125fps )
//    bzr drawer with upate_LEDs but no 20xnops; BT; Audio with 36us readSpectrum-delay: 39fps (audio at 80/200fps )
//    bzr drawer with upate_LEDs but no 20xnops; BT; Audio with 36us delay, inner loop: 39fps (audio at 120/200fps )
//    bzr drawer with upate_LEDs but no 20xnops; BT; Audio with 36us delay, more audio update calls: 39fps (audio at 150/200fps )
//    alienblob drawer with upate_LEDs but no 20xnops; BT; Audio with 36us delay: 26fps (audio at 160/200fps )
//    alienblob drawer with upate_LEDs but no 20xnops; BT; Audio with no delays: 27fps (audio at 160/200fps)
//    alienblob drawer(full FP) with upate_LEDs but no 20xnops; BT; Audio with no delays: 39fps (audio at 160/200fps)

#include <Arduino.h>
#include "Config.h"
#include "Matrix.h"
#include <stdint.h>
#include "gamma2.h"

#ifdef MATRIX_WS2801

#define USE_SPI 0
#if USE_SPI
  #include <SPI.h>
#else
  #define DATAPIN 3   // yellow MOSI(11)
  #define CLOCKPIN 2  // green  SCK(13)
#endif

#define NOP asm("nop\n")
#define NOP10 { NOP; NOP; NOP; NOP; NOP; NOP; NOP; NOP; NOP; NOP; }
#define NOP21 { NOP10; NOP10; NOP; }
#define NOP42 { NOP21; NOP21; }
#define NOP84 { NOP42; NOP42; }

static baseType data[WIDTH][HEIGHT]; // pack 4 bytes per word

#ifdef ARDUINO_DUE
volatile RwReg *dataport, *clockport;
uint32_t datapin, clockpin;
#else
volatile uint8_t *dataport, *clockport;
uint8_t datapin, clockpin;
#endif

// Initialize the matrix
void Matrix_init() {
//#ifdef ARDUINO_DUE
    // allow sychronous PIO writes
    REG_PIOA_OWER = 0xFFFFFFFF;
    REG_PIOB_OWER = 0xFFFFFFFF;
    REG_PIOC_OWER = 0xFFFFFFFF;
    REG_PIOD_OWER = 0xFFFFFFFF;
//#endif
//
#if !USE_SPI
  dataport  = portOutputRegister(digitalPinToPort(DATAPIN));
  clockport = portOutputRegister(digitalPinToPort(CLOCKPIN));    
  datapin   = digitalPinToBitMask(DATAPIN);
  clockpin  = digitalPinToBitMask(CLOCKPIN);

  pinMode(DATAPIN, OUTPUT);
  pinMode(CLOCKPIN, OUTPUT);
#else
  pinMode(10, OUTPUT);
  SPI.begin(10);
  SPI.setClockDivider(10, 84);
  SPI.setDataMode(10, SPI_MODE0);
  SPI.setBitOrder(10, MSBFIRST);
#endif
}

// Send a single RGB 24-bit triplet.
void sendRGB(int x, int y, uint32_t tick, uint32_t tock) {
  uint32_t color = data[x][y];
#if USE_SPI
  byte r = (color >> 16) && 0xFF;
  byte g = (color >> 8) && 0xFF;
  byte b = color && 0xFF;
  SPI.transfer(10, r);
  SPI.transfer(10, g);
  SPI.transfer(10, b);
#else
  // Iterate through color bits (red MSB)
  for(int b=23; b>=0; b--) {
    *clockport = tick;
    //NOP42; // not needed
    if (color & (1<<b)) *dataport |= datapin;
    else *dataport &= ~datapin;
    
    // stall to give the WS2801 time to register the change.
    NOP42; // 42 cycles is ok; 21 is not ok
    
    *clockport = tock;
    NOP42; // 21 cycles is ok; but slight flicker?
    
  }
#endif
}

// Call to refresh the LEDs. Sends all the data to the WS2801s.
void update_LEDs(void (*insideUpdateCallback)()) {
  uint32_t tick = *clockport & ~clockpin;
  uint32_t tock = *clockport | clockpin;  
  for (int y=0; y<HEIGHT; y++) {
      for (int x=0; x<WIDTH; x++) {
        //insideUpdateCallback();
        sendRGB(x, y, tick, tock);
      }
  }
#if !USE_SPI
  *clockport = tick;
#endif
  delayMicroseconds(1000);
}

// set a single pixel
void set_pixel(int x, int y, int r, int g, int b) {
  // take into account how the wall is wired
  y = HEIGHT - 1 - y;
  if (y%2 == 0) {
    x = WIDTH - 1 - x;
  }

//#if DEBUG_LEVEL >= 5
//  if (x==0 && y==0) {
//    p("r="); p(r); p(" g="); p(g); p(" b="); p(b); p("\n");
//  }
//#endif
  
#if PHAGE_WALL
  // necessary only for the Phage Aurora v2
  // set pixel 139 pixel to zero and send it's data to the next pixel
  int n = y*WIDTH + x;
  if (n >= 373) {
    if (n == 373) {
      data[x][y] = 0UL;
    }
    
    n++;
    y = n/WIDTH;
    x = n%WIDTH;
  }
#endif
  
  // gamma correction
#if GAMMA == 25
  r = gamma25[r];
  g = gamma25[g];
  b = gamma25[b];
#endif

  uint32_t dat = b & 0xFF;
  dat |= (g & 0xFF) << 8;
  dat |= (r & 0xFF) << 16;
  data[x][y] = dat;
  //p(x); p(" "); p(y); p("\n");
}
#endif
