/*
The AlienBlob Drawer: waving/oscillating patterns constructed with Perlin noise

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef ALIENBLOB_H
#define ALIENBLOB_H

#include "Drawer.h"
#include "Perlin.h"
#include <math.h>
#include "util.h"

#define FP 16 // fixed point scaling factor exponent

#define ALIENBLOB_SPEED_MULTIPLIER 7

class AlienBlob : public Drawer {
 public:
 AlienBlob(uint16_t width, uint16_t height, DrawerSettings *settings, GlobalSettings *globalSettings) :
  Drawer(width, height, settings, globalSettings) {
    // precalculate 1 period of the sine wave (360 degrees)
    for (int i=0; i<360; i++) {
      float sinVal = sin(2*3.14159*i/360);
      sineTable[i] = float2fix(sinVal, FP);
    }
  }

  int getType() { return DRAWER_ALIENBLOB; }

  void setup() {    
    incr = float2fix(0.03125, FP);
    noiseMult = int2fix(10, FP);
    
    settings->pos = random(100000);
  }

  void reset() { settings->pos = random(100000); }

 void draw(void (*insideDrawCallback)()) {
    Drawer::draw(insideDrawCallback);

    FixedPoint multiplier = float2fix(settings->zoom * 10, FP);
    //float incr2 = incr*multiplier;
    FixedPoint incr2 = multfix(incr, multiplier, FP);
    FixedPoint pi = float2fix(3.14, FP);
    FixedPoint rad_to_deg = divfix(int2fix(180, FP), pi, FP);
    FixedPoint maxColor = int2fix(getNumColors()-1,FP);
    FixedPoint xx;
    FixedPoint yy = 0;
    FixedPoint zz = float2fix(settings->pos / 100.0, FP);
 
    for (int y=0; y<height; y++) {
      xx = 0;
      for (int x=0; x<width; x++) {
        insideDrawCallback();
        
        //float nf = pnoise(xx, yy, zoff);
        FixedPoint ni = pnoise_fp(xx, yy, zz);
        FixedPoint rad = multfix(ni, noiseMult, FP) + 4*pi;
        FixedPoint deg = multfix(rad, rad_to_deg, FP);
        
        int degInt = fix2int(deg,FP) % 360;        
        
        // use pre-calculated sine results
        FixedPoint h = (sineTable[degInt] + int2fix(1,FP)) / 2;
        FixedPoint index = multfix(h, maxColor, FP);;
        int indexInt = fix2int(index,FP);
#if DEBUG_LEVEL >= 5
        if (x==0 && y==0) {
          p("Alien n="); p(fix2float(ni,FP)); p(" sin("); p(degInt); p(") -> "); p(indexInt); p("\n");
        }
#endif        
	             
        // determine pixel color
        setColor(x,y,indexInt);
         
        xx += incr2;
      }
       
      yy += incr2;
    }
     
    // move through noise space -> animation
    //zoff += zoffIncr2;
  }
  
 private:
  //float zoff, incr;
  FixedPoint incr;
  FixedPoint noiseMult;
  FixedPoint sineTable[360];
};

#endif
