/*
The "off" drawer: don't display anything

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef EQUALIZERDRAWER_H
#define EQUALIZERDRAWER_H

#include <stdint.h>
#include "Drawer.h"

class EqualizerDrawer : public Drawer {
 public:
  EqualizerDrawer(uint16_t width, uint16_t height, DrawerSettings *settings, GlobalSettings *globalSettings) :
    Drawer(width, height, settings, globalSettings) {
  }

  void setup() {}
  void reset() {}
  int getType() { return DRAWER_EQUALIZER; }

  void draw(void (*insideDrawCallback)()) {
    Drawer::draw(insideDrawCallback);
    insideDrawCallback();

//    static int iter=0;
//    if (iter % 500 == 0) {
//      Serial.print(lowIntensity); Serial.print(" "); Serial.print(midIntensity); Serial.print(" "); Serial.println(highIntensity);
//      
//      update_LEDs();
//    }
//    iter++;

    colorLedRows(0, 10, 255, 0, 0, findBeats->getBeatRecentness(1));
    colorLedRows(11, 20, 0, 255, 0, findBeats->getBeatRecentness(3));
    colorLedRows(21, 31, 0, 0, 255, findBeats->getBeatRecentness(5));
  }

  void colorLedRows(byte startCol, byte endCol, byte r, byte g, byte b, byte intensity) {
    byte r2 = int(r) * intensity / 255;
    byte g2 = int(g) * intensity / 255;
    byte b2 = int(b) * intensity / 255;
    
    for (int x=startCol; x<=endCol; x++) {
      for (int y=0; y<HEIGHT; y++) {
        set_pixel(x, y, r2, g2, b2);
      }
    }
  }
};

#endif
