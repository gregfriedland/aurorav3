/* Copyright (c) 2012 the authors listed at the following URL, and/or
the authors of referenced articles or incorporated external code:
http://en.literateprograms.org/RGB_to_HSV_color_space_conversion_(C)?action=history&offset=20110802173944

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Retrieved from: http://en.literateprograms.org/RGB_to_HSV_color_space_conversion_(C)?oldid=17206
*/

#include <stdint.h>
#include "util.h"

#define MIN3(x,y,z)  ((y) <= (z) ? \
                         ((x) <= (y) ? (x) : (y)) \
                     : \
                         ((x) <= (z) ? (x) : (z)))

#define MAX3(x,y,z)  ((y) >= (z) ? \
                         ((x) >= (y) ? (x) : (y)) \
                     : \
                         ((x) >= (z) ? (x) : (z)))

void rgbToHsv(uint8_t r, uint8_t g, uint8_t b, uint16_t *h, uint16_t *s, uint16_t *v) {
  uint8_t rgb_min, rgb_max;
  rgb_min = MIN3(r, g, b);
  rgb_max = MAX3(r, g, b);
  *v = rgb_max;
  if (*v == 0) {
    *h = *s = 0;
    return;
  }
  *s = 255*long(rgb_max - rgb_min)/(*v);
  if (*s == 0) {
    *h = 0;
  } else if (rgb_max == r) {
    *h = 0 + 43*(g - b)/(rgb_max - rgb_min);
  } else if (rgb_max == g) {
    *h = 85 + 43*(b - r)/(rgb_max - rgb_min);
  } else /* rgb_max == b */ {
    *h = 171 + 43*(r - g)/(rgb_max - rgb_min);
  }
}



void hsvToRgb(uint8_t *r, uint8_t *g, uint8_t *b, uint16_t hue, uint16_t sat, uint16_t val){
	unsigned int H_accent = hue/60;
	unsigned int bottom = ((255 - sat) * val)>>8;
	unsigned int top = val;
	unsigned char rising  = ((top-bottom)  *(hue%60   )  )  /  60  +  bottom;
	unsigned char falling = ((top-bottom)  *(60-hue%60)  )  /  60  +  bottom;

	switch(H_accent) {
	case 0:
		*r = top;
		*g = rising;
		*b = bottom;
		break;

	case 1:
		*r = falling;
		*g = top;
		*b = bottom;
		break;

	case 2:
		*r = bottom;
		*g = top;
		*b = rising;
		break;

	case 3:
		*r = bottom;
		*g = falling;
		*b = top;
		break;

	case 4:
		*r = rising;
		*g = bottom;
		*b = top;
		break;

	case 5:
		*r = top;
		*g = bottom;
		*b = falling;
		break;
	}
}


// put a lower bound on the saturation or shift hue 
void alterHSV(uint8_t *r, uint8_t *g, uint8_t *b, uint8_t minSaturation, uint8_t hueShift) {
  if (minSaturation > 0 || hueShift > 0) {
    //printf2("%3d %3d %3d ", r,g,b);
    uint16_t h,s,v;
    rgbToHsv(*r,*g,*b,&h,&s,&v);
    s = MAX(minSaturation, s);
    h = (h + hueShift) % 360;
    hsvToRgb(r,g,b,h,s,v);
    //printf2("%3u %3u %3u %3u %3u %3u\n", h,s,v,r,g,b);
  }
}
