/*
Define an abstract base class for Drawers and implement DrawerSettings

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef DRAWER_H
#define DRAWER_H

#include <stdint.h>
#include "Config.h"
#include "RGB_HSV.h"
#include "FindBeats.h"
#include "Palette.h"
#include <Arduino.h>
#include "Matrix.h"

// Define the supported drawers
enum DrawerType {
  DRAWER_BZR = 0,
  DRAWER_ALIENBLOB = 1,
  DRAWER_TEXT = 2,
  DRAWER_PAINT = 3,
  DRAWER_TEST = 4,
  DRAWER_PHAGE = 5,
  DRAWER_OFF = 6,
  DRAWER_EQUALIZER = 7,
  DRAWER_BEATBALLS = 8,
  MAX_USED_DRAWER = 9,
  // Put unused drawers down here
};

class GlobalSettings {
public:
  char text[MAX_TEXT_LENGTH];
};


class DrawerSettings {
 public:
  DrawerSettings(float _speed, float _colorCycling, float _zoom, float _custom, Palette *_palette, int _speedMultiplier) 
    : speed(_speed), colorCycling(_colorCycling), zoom(_zoom), custom(_custom), palette(_palette), speedMultiplier(_speedMultiplier) {
      minSaturation = 0; hueShift = 0; }
  Palette *getPalette() { return palette; }
  
  void setPalette(Palette *_palette) { palette = _palette; }
  
  // is the given touch index active? if so, set the x and y coordinates.
  // a touch is active if it hasn't been IS_TOUCHING_TIMEOUT ms since the message was sent.
  bool isTouching(int touchNum, int *x, int *y) {
    if (millis() - lastTouchTimer[touchNum] < IS_TOUCHING_TIMEOUT) {
      *x = touchX[touchNum];
      *y = touchY[touchNum];
      return true;
    }
    return false;
  }

  // indicate that a touch is currently occurring
  void setTouch(int touchNum, int x, int y) {
    if (x >= 0 && x < WIDTH && y >= 0 && y < HEIGHT) {
      touchX[touchNum] = x;
      touchY[touchNum] = y;
      lastTouchTimer[touchNum] = millis();
    }
  }
  
  float pos;
  int colorIndex, hueShift, minSaturation;
  float speed, colorCycling, zoom, custom;
  int speedMultiplier;
  Palette *palette;
  
 private:
  uint8_t touchX[MAX_TOUCHES], touchY[MAX_TOUCHES];
  uint32_t lastTouchTimer[MAX_TOUCHES];  
};


class Drawer {
 public:
  virtual void setup() = 0;
  virtual void reset() = 0;
  virtual int getType() = 0;
  int getNumColors() { return PALETTE_SIZE; }
  DrawerSettings *getSettings() { return settings; }  

  virtual void draw(void (*insideDrawCallback)()) {
    float speed = settings->speed;
    float colorCycling = settings->colorCycling;

#if USE_AUDIO
    // low freq: reduce speed when beating
    byte lowBeatRecentness = findBeats->getBeatRecentness(BEAT_LOW_BAND);
    byte lowBeatCenter = 128 - abs(lowBeatRecentness - 128); // ranges from 0 -> 128
    if (lowBeatCenter > 0) {
      speed *= float(128 - lowBeatCenter)/128;
    }
#endif
    
    settings->pos += speed * settings->speedMultiplier;
    int colorCyclingMultiplier = getNumColors()/40;
    settings->colorIndex += colorCycling * colorCyclingMultiplier;
  }
  
  Drawer(uint16_t w, uint16_t h, DrawerSettings *_settings, GlobalSettings *_globalSettings) {
    width = w;
    height = h;
    settings = _settings;
    globalSettings = _globalSettings;
    settings->pos = 0;
    settings->colorIndex = 0;
  }
  
  void setColor(int x, int y, int index) {
    if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT) return;

    int hueShift = settings->hueShift;
    
    index = (index + settings->colorIndex + 2*getNumColors()) % getNumColors();

//#if USE_AUDIO
//    // high
//    byte freqBands[] = {BEAT_LOW_BAND, BEAT_MID_BAND, BEAT_HIGH_BAND};
//    for (byte i=0; i<3; i++) {
//      byte band = freqBands[i];
//      byte beatRecentness = findBeats->getBeatRecentness(band);
//      byte beatCenter = 128 - abs(beatRecentness - 128); // time space
//
//      // define a ramp function with a center at colorShiftCenterIndex
//      if (beatCenter > 0) {
//        int rampRange = 256;
//        int rampStart = rampRange*i;
//        int rampCenter = rampStart + rampRange/2;
//        int rampEnd = rampRange * (i+1);
//        
//        int indexIncr;
//        if (index >= rampStart && index < rampCenter) {
//          indexIncr = (index - rampStart) * 6 * beatCenter / rampRange; // up
//        } else if (index >= rampCenter && index < rampEnd) {
//          indexIncr = (rampEnd - index) * 6 * beatCenter / rampRange; // up
//        }
//        
//        index = (index + indexIncr) % getNumColors();
//      }
//    }
//#endif
    
    Palette *palette = settings->getPalette();
    uint8_t r = palette->r[index];
    uint8_t g = palette->g[index];
    uint8_t b = palette->b[index];
//    if (x==0 && y==0) {
//      p(settings->minSaturation); p(" "); p(r); p(" "); p(g); p(" "); p(b); p(" ");
//    }
    alterHSV(&r,&g,&b, settings->minSaturation, hueShift);

#if DEBUG_LEVEL >= 3
    if (x==30 && y==WIDTH-1 && (r != 0 || g != 0 || b != 0)) {
      p("x==30; y==WIDTH-1: "); p(r); p(" "); p(g); p(" "); p(b); p("\n");
    }
#endif
    
    set_pixel(x, y, r, g, b);
  }
  
  void setColor(int x, int y, uint8_t r, uint8_t g, uint8_t b) {
    //p(x); p(" "); p(y); p("\n");
    if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT) return;
    set_pixel(x,y,r,g,b);
  }
  
  void setAll(uint8_t r, uint8_t g, uint8_t b) {
    for (int x=0; x<width; x++) {
      for (int y=0; y<height; y++) {
        set_pixel(x,y,r,g,b);
      }
    }  
  }
  
  void setFindBeats(FindBeats *_findBeats) {
    findBeats = _findBeats;
  }
  
 protected:
  uint16_t width, height;
  DrawerSettings *settings;
  GlobalSettings *globalSettings;
#if USE_AUDIO
  FindBeats *findBeats;
#endif
};

#endif
