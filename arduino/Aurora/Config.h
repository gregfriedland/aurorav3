//
//  Config.h
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#ifndef Config_h
#define Config_h

#include <stdint.h>


////////////////// Settings for configuring the Phage/Langton LED wall ////////////////////
#define PHAGE_WALL 1
#define START_DRAWER DRAWER_BZR
////////////////////////////////////////////////////////////////////////////////////////


#if PHAGE_WALL
  #define FPS 40  // the desired frames per second
  #define WIDTH 32 // width of the display
  #define HEIGHT 17 // height of the display

  #define USE_BLUETOOTH 0
  #define USE_SERIAL 1
  #define USE_AUDIO 1
#else
  #define FPS 40  // the desired frames per second
  #define WIDTH 32 // width of the display
  #define HEIGHT 17 // height of the display

  #define USE_BLUETOOTH 1
  #define USE_SERIAL 0
  #define USE_AUDIO 0
#endif


#define ARDUINO_DUE
typedef uint32_t baseType;


// define the different supported matrix types and pick one to use
//#define MATRIX_ADAFRUIT 0
#define MATRIX_WS2801


#define GAMMA 25 // 10x the gamma value used; this cant be changed willynilly; the table must be generated for the given value
#define STATUS_OUTPUT_TIME 5000000 // how often in micros to output the BT status and effective FPS
#define MAX_TEXT_LENGTH 25000 // the longest text allowed
#define MAX_TEXT_LINES 256
#define MAX_TOUCHES 5 // max simultaneous touches tracked
#define IS_TOUCHING_TIMEOUT 100 // # of milliseconds before a touch expires
#define BT_SEND_SETTINGS_DELAY 3000000 // how long to wait in microseconds before sending the BT settings

#define NUM_PALETTES 201 // how many palettes exist in the palette array
#define PALETTE_SIZE 1024 // the number of colors to put in each palette
#define MIN_SATURATION 0 // min saturation to use for base colors
#define PALETTE_DURATION 120000UL // how often to change the palette



#define DEBUG_LEVEL 1
#define p(...) Serial.print(__VA_ARGS__)

#if USE_SERIAL
  #define SERIAL_BAUD 57600
#endif

#if USE_AUDIO
  #define ANALOG_PIN A0
  #define RESET_PIN 5
  #define STROBE_PIN 4

  #define BEAT_HOLD_TIME 300    // how long to hold the beat for
  #define BEAT_WAIT_TIME 350    // how many ms to wait before allowing another beat
  #define DEFAULT_SENSITIVITY 5 // how sensitive to be when looking for beats; higher is more sensitive
  #define UPDATE_PERIOD 5       // the most often time interval that beats are updated: 5ms is a good number; 20 is too high; lower uses more RAM

  #define NUM_BANDS 7
  #define BEAT_LOW_BAND 1
  #define BEAT_MID_BAND 3
  #define BEAT_HIGH_BAND 5
#endif

#endif
