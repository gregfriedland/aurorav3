//
//  platform.cpp
//  ArduinoV3
//
//  Created by Greg Friedland on 7/20/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#include "platform.h"


#ifdef __MK20DX128__ // Teensy 3.0

#else ifdef __SAM3X8E__ // Arduino Due
  #include <DueTimer.h>

  // used by scheduleCallback
  CallbackFunc lastCallback;
  void callbackWrapper() {
    lastCallback();
    Timer3.stop();
  }

  // schedule a function to be called once after a delay
  void scheduleCallback(CallbackFunc callback, int microsDelay) {
    lastCallback = callback;
    Timer3.attachInterrupt(callbackWrapper).start(microsDelay);
  }

  // Get random numbers on the Due that are better than those seeded with anologRead; from
  // http://arduino.cc/forum/index.php?topic=129083.0
  uint32_t trueRandom() {
    static bool enabled = false;
    if (!enabled) {
      pmc_enable_periph_clk(ID_TRNG);
      TRNG->TRNG_IDR = 0xFFFFFFFF;
      TRNG->TRNG_CR = TRNG_CR_KEY(0x524e47) | TRNG_CR_ENABLE;
      enabled = true;
    }
    
    while (! (TRNG->TRNG_ISR & TRNG_ISR_DATRDY))
      ;
    return TRNG->TRNG_ODATA;
  }

// start the RNG by setting the seed
  void startRNG() {
    randomSeed(trueRandom());
  }




  #if 0
  #ifdef ARDUINO_DUE
  // run the connected callback in 1s
  startTimer(TC0, 0, TC0_IRQn, 1);
  #endif

  #ifdef ARDUINO_DUE
  // Timer handler for the connect callback
  void TC0_Handler() {
    TC_GetStatus(TC0, 0);
    connCallback();
    TC_Stop(TC0, 0); // only call it once
  }
  #endif

  // Start the timer on the Due
  void startTimer(Tc *tc, uint32_t channel, IRQn_Type irq, uint32_t frequency) {
    pmc_set_writeprotect(false);
    pmc_enable_periph_clk((uint32_t)irq);
    TC_Configure(tc, channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | TC_CMR_TCCLKS_TIMER_CLOCK4);
    uint32_t rc = VARIANT_MCK/128/frequency; //128 because we selected TIMER_CLOCK4 above
    tc->TC_CHANNEL[channel].TC_IER=TC_IER_CPCS;
    tc->TC_CHANNEL[channel].TC_IDR=~TC_IER_CPCS;
    NVIC_EnableIRQ(irq);
    
    TC_Stop(tc, channel);
    TC_SetRC(tc, channel, rc);
    TC_Start(tc, channel);
  }
#endif

#endif