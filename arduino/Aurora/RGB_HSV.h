#ifndef RGB_HSV_H
#define RGB_HSV_H

void rgbToHsv(uint8_t r, uint8_t g, uint8_t b, uint16_t *h, uint16_t *s, uint16_t *v);
void hsvToRgb(uint8_t *r, uint8_t *g, uint8_t *b, uint16_t hue, uint16_t sat, uint16_t val);
void alterHSV(uint8_t *r, uint8_t *g, uint8_t *b, uint8_t minSaturation, uint8_t hueShift);

#endif
