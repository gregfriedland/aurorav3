/*
Utility functions
*/

#include "util.h"

// calculate distance between two points
float dist(float x1, float y1, float x2, float y2) {
  float dx = x1-x2;
  float dy = y1-y2;
  return sqrt(dx*dx + dy*dy);
}


// interpolate between two ints
uint16_t interp(uint8_t start, uint8_t end, uint8_t index, uint8_t length) {
  return ((uint16_t)end)*index/length + ((uint16_t)start)*(length-index)/length;
}

