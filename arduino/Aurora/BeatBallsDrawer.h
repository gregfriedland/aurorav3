/*
 The "off" drawer: don't display anything
 
 Copyright (C) 2013 Greg Friedland
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef BEATBALSDRAWER_H
#define BEATBALLSDRAWER_H

#include <stdint.h>
#include "Drawer.h"

#define MAX_BALLS 20

class Ball {
public:
  Ball() { radius = x = y = r = g = b = lastBeatTime = 0; }
  byte radius;
  byte x, y;
  byte r,g,b;
  byte band;
  uint32_t lastBeatTime;
  byte intensity;
};

class BeatBallsDrawer : public Drawer {
public:
  BeatBallsDrawer(uint16_t width, uint16_t height, DrawerSettings *settings, GlobalSettings *globalSettings) :
  Drawer(width, height, settings, globalSettings) {}
  
  void setup() {}
  void reset() {}
  int getType() { return DRAWER_BEATBALLS; }
  
  void drawBall(Ball *ball) {
    if (ball->intensity <= 25) return;
    
    byte minx = MAX(0, int(ball->x) - ball->radius);
    byte maxx = MIN(WIDTH-1, ball->x + ball->radius);
    byte miny = MAX(0, int(ball->y) - ball->radius);
    byte maxy = MIN(HEIGHT-1, ball->y + ball->radius);
    int r2 = ball->radius * ball->radius;
    
    byte r = int(ball->r) * ball->intensity / 255;
    byte g = int(ball->g) * ball->intensity / 255;
    byte b = int(ball->b) * ball->intensity / 255;
    
    for (int x=minx; x<=maxx; x++) {
      for (int y=miny; y<=maxy; y++) {
        if ((x-ball->x)*(x-ball->x) + (y-ball->y)*(y-ball->y) <= r2) {
          set_pixel(x, y, r, g, b);
        }
      }
    }
  }
  
  void updateBalls(void (*insideDrawCallback)()) {
    byte beatBands[3] = {BEAT_LOW_BAND, BEAT_MID_BAND, BEAT_HIGH_BAND};
    
    // decay intensity on all balls
    for (int b=0; b<MAX_BALLS; b++) {
      balls[b].intensity *= 0.97;
    }

    
    for (int band=0; band<NUM_BANDS; band++) {
      if (!findBeats->getBandEnabled(band)) continue;
      // have we assigned a ball to this recent beat yet?
      bool found = false;
      for (int b=ballIndex-MAX_BALLS; b<ballIndex; b++) {
        Ball *currBall = &balls[(b+MAX_BALLS)%MAX_BALLS];
//        if (currBall->band == band) {
//          if (currBall->lastBeatTime == findBeats->getLastBeatTime(band)) {
//            found = true;
//          }
//        }
        if (abs((int64_t)findBeats->getLastBeatTime(band) - currBall->lastBeatTime) < 250) found = true;
      }
      
      // we haven't assigned one yet so do that now
      if (!found) {
        Ball *currBall = &balls[ballIndex];
        currBall->lastBeatTime = findBeats->getLastBeatTime(band);
        //currBall->intensity = findBeats->getBeatRecentness(band);
        currBall->intensity = 255; //int(findBeats->getEnergy(band, 250)); // * findBeats->getBeatRecentness(band) / 255;
        currBall->band = band;
        //currBall->colorIndex = getNumColors() * i / 3;
        currBall->radius = map(findBeats->getBeatSharpness(band), 0, 255, 2, 6);
        currBall->x = random(WIDTH);
        currBall->y = random(HEIGHT);
        currBall->r = 255*(band==BEAT_LOW_BAND);
        currBall->g = 255*(band==BEAT_MID_BAND);
        currBall->b = 255*(band==BEAT_HIGH_BAND);
        ballIndex++;
        if (ballIndex == MAX_BALLS) {
          ballIndex = 0;
        }
      }
    }
  }
  
  void draw(void (*insideDrawCallback)()) {
    Drawer::draw(insideDrawCallback);
    insideDrawCallback();
 
    updateBalls(insideDrawCallback);
    setAll(0,0,0);
    for (int b=0; b<MAX_BALLS; b++) {
      drawBall(&balls[b]);
    }
  }
  
  
private:
  Ball balls[MAX_BALLS];
  byte ballIndex = 0;
};

#endif
