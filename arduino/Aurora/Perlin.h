/*
 Ken Perlins improved noise   -  http://mrl.nyu.edu/~perlin/noise/
 C-port:  http://www.fundza.com/c4serious/noise/perlin/perlin.html
 by Malcolm Kesson;   arduino port by Peter Chiochetti, Sep 2007 :
 -  make permutation constant byte, obsoletes init(), lookup % 256
*/

#include <math.h>

static const byte p[] = {   151,160,137,91,90, 15,131, 13,201,95,96,
53,194,233, 7,225,140,36,103,30,69,142, 8,99,37,240,21,10,23,190, 6,
148,247,120,234,75, 0,26,197,62,94,252,219,203,117, 35,11,32,57,177,
33,88,237,149,56,87,174,20,125,136,171,168,68,175,74,165,71,134,139,
48,27,166, 77,146,158,231,83,111,229,122, 60,211,133,230,220,105,92,
41,55,46,245,40,244,102,143,54,65,25,63,161, 1,216,80,73,209,76,132,
187,208, 89, 18,169,200,196,135,130,116,188,159, 86,164,100,109,198,
173,186, 3,64,52,217,226,250,124,123,5,202,38,147,118,126,255,82,85,
212,207,206, 59,227, 47,16,58,17,182,189, 28,42,223,183,170,213,119,
248,152,2,44,154,163,70,221,153,101,155,167,43,172, 9,129,22,39,253,
19,98,108,110,79,113,224,232,178,185,112,104,218,246, 97,228,251,34,
242,193,238,210,144,12,191,179,162,241,81,51,145,235,249,14,239,107,
49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,
150,254,138,236,205, 93,222,114, 67,29,24, 72,243,141,128,195,78,66,
215,61,156,180
};

float fade(float t){ return t * t * t * (t * (t * 6 - 15) + 10); }
float lerp(float t, float a, float b){ return a + t * (b - a); }
float grad(int hash, float x, float y, float z)
{
int     h = hash & 15;          /* CONVERT LO 4 BITS OF HASH CODE */
float  u = h < 8 ? x : y,      /* INTO 12 GRADIENT DIRECTIONS.   */
          v = h < 4 ? y : h==12||h==14 ? x : z;
return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

#define P(x) p[(x) & 255]

float pnoise(float x, float y, float z)
{
int   X = (int)floor(x) & 255,             /* FIND UNIT CUBE THAT */
      Y = (int)floor(y) & 255,             /* CONTAINS POINT.     */
      Z = (int)floor(z) & 255;
x -= floor(x);                             /* FIND RELATIVE X,Y,Z */
y -= floor(y);                             /* OF POINT IN CUBE.   */
z -= floor(z);
float  u = fade(x),                       /* COMPUTE FADE CURVES */
        v = fade(y),                       /* FOR EACH OF X,Y,Z.  */
        w = fade(z);
        
//if (X==0 && Y==0 && x==0 && y==0) {
//  Serial.print(X); Serial.print(" "); Serial.print(x); Serial.print(" "); Serial.print(Y); Serial.print(" "); 
//  Serial.print(y); Serial.print(" "); Serial.print(Z); Serial.print(" "); Serial.print(z); Serial.print(": "); Serial.println(w);
//}

int  A = P(X)+Y, 
     AA = P(A)+Z, 
     AB = P(A+1)+Z,                        /* HASH COORDINATES OF */
     B = P(X+1)+Y, 
     BA = P(B)+Z, 
     BB = P(B+1)+Z;                        /* THE 8 CUBE CORNERS, */

return lerp(w,lerp(v,lerp(u, grad(P(AA  ), x, y, z),   /* AND ADD */
                          grad(P(BA  ), x-1, y, z)),   /* BLENDED */
              lerp(u, grad(P(AB  ), x, y-1, z),        /* RESULTS */
                   grad(P(BB  ), x-1, y-1, z))),       /* FROM  8 */
            lerp(v, lerp(u, grad(P(AA+1), x, y, z-1),  /* CORNERS */
                 grad(P(BA+1), x-1, y, z-1)),          /* OF CUBE */
              lerp(u, grad(P(AB+1), x, y-1, z-1),
                   grad(P(BB+1), x-1, y-1, z-1))));
}

// x, y, z, num harmonics, unused, harmonic falloff
float pnoise_harmonics(float x, float y, float z, int octaves, float freq, float decay) {
  float scale = 0;
  float result = 0;
  for (int i=0; i<octaves; i++ ) {
    scale += decay;
    result += pnoise(x, y, z) * decay;
    x *= freq;
    y *= freq;
  }

  return result / scale;
}



// ***************** fixed point arithmetic ***************** 


// From http://mrl.nyu.edu/~perlin/noise/INoise.java
// Copyright 2002 Ken Perlin
// FIXED POINT VERSION OF IMPROVED NOISE:  1.0 IS REPRESENTED BY 2^16

int p_fp[512];
int fade_fp_arr[256];
boolean perlin_fp_inited = false;

int lerp_fp(int t, int a, int b) { return a + (t * (b - a) >> 12); }
 
int grad_fp(int hash, int x, int y, int z) {
  int h = hash&15, u = h<8?x:y, v = h<4?y:h==12||h==14?x:z;
  return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}
 
static const byte permutation_fp[] = { 151,160,137,91,90,15,
   131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
   190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
   88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
   77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
   102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
   135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
   5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
   223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
   129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
   251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
   49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
   138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};
 
int fade_fp(int t) {
  int t0 = fade_fp_arr[t >> 8], t1 = fade_fp_arr[min(255, (t >> 8) + 1)];
  return t0 + ( (t & 255) * (t1 - t0) >> 8 );
}

double f(double t) { return t * t * t * (t * (t * 6 - 15) + 10); }

void pnoise_init_fp() {
 for (int i=0; i < 256 ; i++) p_fp[256+i] = p_fp[i] = permutation_fp[i];
 for (int i=0; i < 256 ; i++) fade_fp_arr[i] = (int)((1<<12)*f(i/256.));
}

int pnoise_fp(int x, int y, int z) {
    if (!perlin_fp_inited) {
      pnoise_init_fp();
      perlin_fp_inited = true;
    }
  
    int X = x>>16 & 255, Y = y>>16 & 255, Z = z>>16, N = 1<<16;
    x &= N-1; y &= N-1; z &= N-1;
    int u=fade_fp(x),v=fade_fp(y),w=fade_fp(z), A=p[X  ]+Y, AA=p[A]+Z, AB=p[A+1]+Z,
                                       B=p[X+1]+Y, BA=p[B]+Z, BB=p[B+1]+Z;
    return lerp_fp(w, lerp_fp(v, lerp_fp(u, grad_fp(p[AA  ], x   , y   , z   ),  
                                   grad_fp(p[BA  ], x-N , y   , z   )), 
                           lerp_fp(u, grad_fp(p[AB  ], x   , y-N , z   ),  
                                   grad_fp(p[BB  ], x-N , y-N , z   ))),
                   lerp_fp(v, lerp_fp(u, grad_fp(p[AA+1], x   , y   , z-N ),  
                                   grad_fp(p[BA+1], x-N , y   , z-N )), 
                           lerp_fp(u, grad_fp(p[AB+1], x   , y-N , z-N ),
                                   grad_fp(p[BB+1], x-N , y-N , z-N ))));
}
 

// x, y, z, num harmonics, unused, harmonic falloff
int pnoise_harmonics_fp(int x, int y, int z, int octaves, int freq, int decay) {
  int result = 0;
  for (int i=0; i<octaves; i++ ) {
    result += pnoise(x, y, z) / decay;
    x *= freq;
    y *= freq;
  }

  return result;
}




