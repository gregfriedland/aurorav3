//
//  Commands.h
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#ifndef Commands_H
#define Commands_H


// Define the command types; the structure is 1-byte command, multi-byte payload, end with 255
enum Command {
  CMD_NONE         = 0x00, // noop; e.g. can be used to send BTLE 'credits' from iOS to the BTLE shield
  CMD_NEXT_PROGRAM = 0x01, // one arg (up/down)
  CMD_NEXT_PALETTE = 0x02, // one arg (up/down)
  CMD_CLEAR_TEXT   = 0x03, // text string in payload; text entry on ipad
  CMD_SET_SPEED    = 0x04, // one arg
  CMD_SET_COLORCYCLING    = 0x05, // one arg
  CMD_SET_ZOOM  = 0x06, // one arg
  CMD_SET_CUSTOM  = 0x07, // one arg
  CMD_RESET        = 0x08, // one arg (up/down)
  CMD_SET_BRIGHTNESS = 0x09, // one arg
  CMD_SET_MIN_SATURATION = 0x0A, // one arg
  CMD_SET_PROGRAM    = 0x0B, // one arg
  CMD_SET_PALETTE    = 0x0C, // one arg
  CMD_SET_POSITION    = 0x0D, // one arg
  CMD_SET_COLOR_INDEX    = 0x0E, // one arg
  CMD_SET_HUE_SHIFT    = 0x0F, // one arg
  
  CMD_ENABLE_AUDIO = 0x10, // two args
  CMD_SET_AUDIO_SENSITIVITY = 0x11, // two args
  CMD_APPEND_TEXT  = 0x12, // text string in payload; text entry on ipad
  CMD_TEXT_RECEIVED  = 0x13, // text string was received, ready to receive next one
  
  CMD_SET_TOUCH = 0x17, // three args
  CMD_HEARTBEAT = 0x1C, // no args
  MAX_CMD = 0x1D,
};
#define MAX_ARG 254 // maximum command argument value since 255 is a special value

void cmds_setup();
void cmds_loop();
void cmds_sendSettings();
void cmds_updatePalette();

#endif 
