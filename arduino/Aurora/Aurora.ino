  
/*
Main entry point for the revised Aurora LED wall written to run on an Arduino Due.

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

// TODO
// -music responsiveness
// -sending lots of text over serial?
// -findBeats speed

// libraries used by submodules have to be included in the main .ino file
#include <SPI.h>
#include <DueTimer.h>
#if USE_BLUETOOTH
  #include <ble_system.h>
  #include <ble_shield_uart.h>
#endif

#include "Aurora.h"
#include "Config.h"
#include "bzr.h"
#include "Drawer.h"
#include "AlienBlob.h"
#include "OffDrawer.h"
#include "TestDrawer.h"
#include "TextDrawer.h"
#include "PaintDrawer.h"
#include "PhageDrawer.h"
#include "EqualizerDrawer.h"
#include "BeatBallsDrawer.h"
#include "util.h"
#include "FindBeats.h"
#include "Commands.h"
#include "TextStrings.h"



DrawerSettings *settings[MAX_USED_DRAWER]; // stores all the drawer settings
Drawer *drawer = NULL;  // pointer to the current drawer
Palette *palette; // the current color palette

uint32_t lastPrintTime, lastUpdateTime, interval, iter;


GlobalSettings globalSettings;


#if USE_AUDIO
  bool beatsEnabled[NUM_BANDS] = {0,1,0,1,0,1,0};
  FindBeats findBeats(UPDATE_PERIOD);
#endif

// Define a callback that should be called often inside of drawers. Not too often!
// This callback must be 'nice' and not take too much CPU away from the drawer.
void callOftenCallback() {
#if USE_AUDIO
  // The beats must be updated often (~200fps) to work
  findBeats.updateBeats();
#endif
}

void setup() {
  Serial.begin(115200);

  Matrix_init();

  lastPrintTime = micros();
  lastUpdateTime = micros();
  interval = 1000000 / FPS;
  iter = 0;  
  
  randomSeed(trueRandom());

  palette = (Palette*) malloc(sizeof(Palette));
  int paletteIndex = random(NUM_PALETTES);
  loadPalette(paletteIndex, palette);
#if DEBUG_LEVEL >= 1
  p("Pal "); p(paletteIndex); p("\n");
#endif
  
  cmds_setup();

  createDefaultDrawerSettings();
  changeDrawer(START_DRAWER);
  
  
#if USE_AUDIO
  for (byte b=0; b<NUM_BANDS; b++) {
    if (!beatsEnabled[b]) continue;
    findBeats.setBandEnabled(b, true);
    findBeats.setBandSensitivity(b, DEFAULT_SENSITIVITY);
  }
  findBeats.setBeatWaitTime(BEAT_WAIT_TIME);
  findBeats.setBeatHoldTime(BEAT_HOLD_TIME);
  findBeats.init(ANALOG_PIN, STROBE_PIN, RESET_PIN);
#endif
  
#if DEBUG_LEVEL >= 1
  p("Setup done\n");
#endif
}
     
void loop() {
  callOftenCallback();
  
  unsigned int newTime = micros();  
  if (newTime - lastUpdateTime > interval) {
    lastUpdateTime = newTime;

    drawer->draw(callOftenCallback);
    update_LEDs(callOftenCallback);
    callOftenCallback();

    // output FPS every so often
#if DEBUG_LEVEL >= 1
    iter++;
    if (newTime - lastPrintTime >= STATUS_OUTPUT_TIME) {
      int diff = newTime - lastPrintTime;
      p(iter * 1000000 / diff); p(" fps ");
      lastPrintTime = newTime;
      iter = 0;
    }
#endif
  }
  
  cmds_updatePalette();
  cmds_loop();
}


// change the drawer and free up the old one's memory
void changeDrawer(int drawerType) {
  if (drawer != NULL) {
    delete drawer;
    drawer = NULL;
  }
    
  switch(drawerType) {
    case DRAWER_TEXT:
      drawer = new TextDrawer(WIDTH, HEIGHT, settings[DRAWER_TEXT], &globalSettings);
      break;
    case DRAWER_BZR:
      drawer = new Bzr(WIDTH, HEIGHT, settings[DRAWER_BZR], &globalSettings);
      break;
    case DRAWER_TEST:
      drawer = new TestDrawer(WIDTH, HEIGHT, settings[DRAWER_TEST], &globalSettings);
      break;
    case DRAWER_ALIENBLOB:
      drawer = new AlienBlob(WIDTH, HEIGHT, settings[DRAWER_ALIENBLOB], &globalSettings);
      break;
    case DRAWER_PAINT:
      drawer = new PaintDrawer(WIDTH, HEIGHT, settings[DRAWER_PAINT], &globalSettings);
      break;
    case DRAWER_PHAGE:
      drawer = new PhageDrawer(WIDTH, HEIGHT, settings[DRAWER_PHAGE], &globalSettings);
      break;
    case DRAWER_OFF:
      drawer = new OffDrawer(WIDTH, HEIGHT, settings[DRAWER_OFF], &globalSettings);
      break;
    case DRAWER_EQUALIZER:
      drawer = new EqualizerDrawer(WIDTH, HEIGHT, settings[DRAWER_EQUALIZER], &globalSettings);
      break;
    case DRAWER_BEATBALLS:
      drawer = new BeatBallsDrawer(WIDTH, HEIGHT, settings[DRAWER_BEATBALLS], &globalSettings);
      break;
  }

#if USE_AUDIO
  drawer->setFindBeats(&findBeats);
#endif
  
  drawer->setup();
  cmds_sendSettings();
  
  drawer->setAll(0,0,0);
  update_LEDs(callOftenCallback);
}

// Initialize drawer settings that look good from the start
void createDefaultDrawerSettings() {
  strcpy(globalSettings.text,startingText);
  
  settings[DRAWER_TEXT] = new DrawerSettings(1.0,0.05,1,1, palette, 1);
  settings[DRAWER_BZR] = new DrawerSettings(0.8, 0.1, 0.63, 1, palette, BZR_SPEED_MULTIPLIER);
  settings[DRAWER_TEST] = new DrawerSettings(0.8, 0.1, 0.63, 1, palette, 1);
  settings[DRAWER_ALIENBLOB] = new DrawerSettings(0.6, 0.1, 0.1, 1, palette, ALIENBLOB_SPEED_MULTIPLIER);
  settings[DRAWER_PAINT] = new DrawerSettings(0,0,0,0, palette, PAINT_SPEED_MULTIPLIER);
  settings[DRAWER_PHAGE] = new DrawerSettings(0.5,0,0,0, palette, PHAGE_SPEED_MULTIPLIER);
  settings[DRAWER_OFF] = new DrawerSettings(0,0,0,0, palette, 1);
  settings[DRAWER_EQUALIZER] = new DrawerSettings(0,0,0,0, palette, 1);
  settings[DRAWER_BEATBALLS] = new DrawerSettings(0,0,0,0, palette, 1);
}



