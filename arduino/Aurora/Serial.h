//
//  Serial.h
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#ifndef Serial_H
#define Serial_H

#include <Arduino.h>

typedef byte BufferType;

#define SERIAL_BUFFER_LENGTH 1024

void serial_setup();
void serial_loop();
int serial_pop_rx_data(BufferType *buffer, int len, int endChar);

#endif
