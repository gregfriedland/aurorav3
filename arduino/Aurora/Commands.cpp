//
//  Commands.cpp
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#include "Commands.h"
#include "Config.h"
#include "util.h"
#include "Drawer.h"
#include "Aurora.h"
#include <Arduino.h>


#if USE_BLUETOOTH
  #include <ble_system.h>
  #include <ble_shield_uart.h>
#else if USE_SERIAL
  #include "Serial.h"
#endif


#if USE_BLUETOOTH
  #define MAX_CMD_ARGS 10 // the max number of char arguments per command
  #define BUFFER_LEN 140
  void BLEConnected() { cmds_sendSettings(); } // callback for establishing a bluetooth connection
  void BLEDisconnected() {} // callback for disconnecting
  byte rcvBuffer[BUFFER_LEN];
#else if USE_SERIAL
  #define MAX_CMD_ARGS 10 // the max number of char arguments per command
  #define BUFFER_LEN 140
  byte rcvBuffer[BUFFER_LEN];
#endif

static uint32_t lastPaletteChgTime;

void cmds_setup() {
#if USE_BLUETOOTH
  ble_setup(BLEConnected, BLEDisconnected);
#endif
#if USE_SERIAL
  serial_setup();
#endif
  
  lastPaletteChgTime = millis();  
}

void sendData(byte *buf, int nBytes) {
#if USE_BLUETOOTH
  if (ble_is_connected()) {
    ble_tx(buf, nBytes);
  }
#endif
  
#if USE_SERIAL
#if DEBUG_LEVEL >= 5
  p("Sending serial data\n");
#endif
  for (int i=0; i<nBytes; i++) {
    Serial1.write(buf[i]);
  }
#endif
}

// callback to process a single command given data in bytes
void processCmd(byte cmd, byte *data, int nbytes) {
  float val, val2;
  int iVal;
  
  int numArgs = nbytes;
  
#if DEBUG_LEVEL >= 2
  p("Processing cmd "); p(cmd); p(" ("); p(nbytes); p(" bytes): ");
  for (int i=0; i<numArgs; i++ ) {
    p(" "); p(data[i], DEC);
  }
  p("\n");
#endif
  
  if (cmd == CMD_CLEAR_TEXT) {
#if DEBUG_LEVEL >= 1
    p("Clearing text\n");
#endif
    globalSettings.text[0] = 0;
    return;
  }
  
  if (cmd == CMD_APPEND_TEXT) {
    if (nbytes < 2) return;
    char *text = globalSettings.text;
    data[nbytes-1] = 0; // replace 255 with string terminator
    int currLen = strlen(text);
    if (currLen + strlen((char*)data) >= MAX_TEXT_LENGTH) return;

    strcpy(text+currLen, (char*)data);

    // tell the ios app that we received the data and it's safe to send more
    byte buf[2] = {CMD_TEXT_RECEIVED, 255};
    sendData(buf, 2);

#if DEBUG_LEVEL >= 2
    p("Text appended ("); p(nbytes); p(" bytes): "); p((char*)data); p("\n");
#endif
    
    // if we're on the text drawer, update the metadata
    if (drawer->getType() == DRAWER_TEXT) {
      drawer->reset();
    }

    return;
  }
  
  if (nbytes >= MAX_CMD_ARGS) {
#if DEBUG_LEVEL >= 1
    p("Rcvd too many args for cmd "); p(cmd); p(": "); p(nbytes); p("\n");
#endif
    return;
  }
  
  // perform the command specific actions
  switch(cmd) {
    case CMD_NONE:
#if DEBUG_LEVEL>=1
      p("Noop\n");
#endif
      break;
      
      //  case CMD_HEARTBEAT:
      //#if DEBUG_LEVEL>=2
      //    p("Heartbeat\n");
      //#endif
      //    lastHeartbeatTime = micros();
      //    break;
      
    case CMD_NEXT_PROGRAM:
#if DEBUG_LEVEL>=1
      p("Next program\n");
#endif
      iVal = (drawer->getType()+1) % MAX_USED_DRAWER;
      changeDrawer(iVal);
      break;
      
    case CMD_RESET:
#if DEBUG_LEVEL>=1
      p("Reset\n");
#endif
      drawer->reset();
      break;
      
    case CMD_SET_SPEED:
      if (numArgs < 1) break;
      //val = data[0] / float(MAX_ARG) * 2 - 1 ;
      val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1
      p("Set speed: "); p(val); p("\n");
#endif
      drawer->getSettings()->speed = val;
      break;
      
    case CMD_SET_COLORCYCLING:
      if (numArgs < 1) break;
      //val = data[0] / float(MAX_ARG) * 2 - 1;
      val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1
      p("Set color cycling: "); p(val); p("\n");
#endif
      drawer->getSettings()->colorCycling = val;
      break;
      
    case CMD_SET_ZOOM:
      if (numArgs < 1) break;
      val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1
      p("Zoom: "); p(val); p("\n");
#endif
      drawer->getSettings()->zoom = val;
      break;
      
    case CMD_SET_CUSTOM:
      if (numArgs < 1) break;
      val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1
      p("Set custom: "); p(val); p("\n");
#endif
      drawer->getSettings()->custom = val;
      break;
      
    case CMD_SET_MIN_SATURATION:
      if (numArgs < 1) break;
      iVal = data[0];
#if DEBUG_LEVEL>=1
      p("Set min saturation: "); p(iVal); p("\n");
#endif
      drawer->getSettings()->minSaturation = iVal;
      break;
      
    case CMD_SET_HUE_SHIFT:
      if (numArgs < 1) break;
      iVal = data[0];
#if DEBUG_LEVEL>=1
      p("Set hue shift: "); p(iVal); p("\n");
#endif
      drawer->getSettings()->hueShift = iVal;  // hue actually goes up to 360
      break;
      
    case CMD_SET_PROGRAM:
      if (numArgs < 1) break;
      iVal = data[0];
      if (iVal >= MAX_USED_DRAWER) break;
#if DEBUG_LEVEL>=1
      p("Set program: "); p(iVal); p("\n");
#endif
      iVal = iVal % MAX_USED_DRAWER;
      changeDrawer(iVal);
      break;
      
    case CMD_SET_PALETTE:
      if (numArgs < 1) break;
      iVal = data[0];
      if (iVal >= NUM_PALETTES) break;
#if DEBUG_LEVEL>=1
      p("Set palette: "); p(iVal); p("\n");
#endif
      loadPalette(iVal, drawer->getSettings()->palette);
      lastPaletteChgTime = millis();
      break;
      
    case CMD_SET_POSITION:
      if (numArgs < 2) break;
      iVal = (data[0] * 255L + data[1]) - 255*127L; // int between -255*127 -> 255*127
#if DEBUG_LEVEL>=1
      p("Set position: "); p(iVal); p("\n");
#endif
      drawer->getSettings()->pos = iVal;
      break;
      
    case CMD_SET_COLOR_INDEX:
      if (numArgs < 2) break;
      iVal = (data[0] * MAX_ARG + data[1]) - MAX_ARG*127L; // int between -254*127 -> 254*127
#if DEBUG_LEVEL>=1
      p("Set color index: "); p(iVal); p("\n");
#endif
      drawer->getSettings()->colorIndex = iVal;
      break;
      
    case CMD_SET_TOUCH:
      if (numArgs < 3) break;
      iVal = data[0];
      if (iVal >= MAX_TOUCHES) break;
      val = round(data[1] / float(MAX_ARG) * WIDTH);
      val2 = round(data[2] / float(MAX_ARG) * HEIGHT);
#if DEBUG_LEVEL>=1
      p("Touch"); p(iVal); p(": "); p(val); p(" "); p(val2); p("\n");
#endif
      drawer->getSettings()->setTouch(iVal, val, val2);
      break;
      
    case CMD_ENABLE_AUDIO:
      if (numArgs < 2 || data[0] >= 7) break;
#if DEBUG_LEVEL>=1
      p("Enable audio"); p(data[0]); p(": "); p(data[1]); p("\n");
#endif
      findBeats.setBandEnabled(data[0], data[1]);
      break;
      
    case CMD_SET_AUDIO_SENSITIVITY:
      if (numArgs < 2 || data[0] >= 7) break;
#if DEBUG_LEVEL>=1
      p("Set audio sensitivity"); p(data[0]); p(": "); p(data[1]); p("\n");
#endif
      findBeats.setBandSensitivity(data[0], data[1]);
      break;
      
    default:
#if DEBUG_LEVEL>=1
      p("Unknown command "); p(cmd); p(": ");
      for (int i=0; i<numArgs; i++) {
        p(data[i], DEC); p(" ");
      }
      p("\n");
#endif
      break;
  }
}

// send the current settings values back to the controller
void cmds_sendSettings() {
  
  byte buf[3] = {CMD_SET_SPEED, int(drawer->getSettings()->speed * MAX_ARG), 255};
  sendData(buf, 3);
  
  byte buf2[3] = {CMD_SET_COLORCYCLING, int(drawer->getSettings()->colorCycling * MAX_ARG), 255};
  sendData(buf2, 3);
  
  byte buf4[3] = {CMD_SET_PALETTE, int(drawer->getSettings()->palette->index * MAX_ARG / NUM_PALETTES), 255};
  sendData(buf4, 3);
  
  byte buf5[4] = {CMD_SET_PROGRAM, drawer->getType(), 0xFF};
  sendData(buf5, 3);
  
#if DEBUG_LEVEL >= 1
  p("Done sending settings\n");
#endif
}


void cmds_loop() {
  int nBytes;
  do {
#if USE_BLUETOOTH
    ble_loop();
    nBytes = ble_pop_rx_data(rcvBuffer, BUFFER_LEN, 0xFF);
    if (nBytes > 0) {
      processCmd(rcvBuffer[0], rcvBuffer+1, nBytes-1);
    }
#else if USE_SERIAL
    serial_loop();
    nBytes = serial_pop_rx_data(rcvBuffer, BUFFER_LEN, 0xFF);
    if (nBytes > 0) {
      processCmd(rcvBuffer[0], rcvBuffer+1, nBytes-1);
    }
#endif
    
  } while (nBytes > 0);
}

void cmds_updatePalette() {
  // change the palette every so often
  unsigned int newTimeMs = millis();
  if (newTimeMs - lastPaletteChgTime > PALETTE_DURATION) {
    int newPalette = random(NUM_PALETTES);
    loadPalette(newPalette, drawer->getSettings()->palette);
    cmds_sendSettings();
    lastPaletteChgTime = newTimeMs;
  }
}
