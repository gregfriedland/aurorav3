//
//  Matrix.h
//  Arduino Arduino
//
//  Created by Greg Friedland on 8/15/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#ifndef Matrix_h
#define Matrix_h


// These are implemented on a per-hardware basis
void set_pixel(int x, int y, int r, int g, int b);
void update_LEDs(void (*insideUpdateCallback)());
void Matrix_init();


#endif
