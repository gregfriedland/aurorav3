/*
Constants and types defined for the Aurora LED wall.

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef AURORA_H
#define AURORA_H

#include <stdint.h>

#if defined(__AVR_ATmega168__) ||defined(__AVR_ATmega168P__) ||defined(__AVR_ATmega328P__)
  #define ARDUINO_UNO
  typedef uint8_t baseType;
  #include <TimerOne.h>
#else
  #define ARDUINO_DUE
  typedef uint32_t baseType;
#endif


// define the different supported matrix types and pick one to use
//#define MATRIX_ADAFRUIT 0
#define MATRIX_WS2801

#define FPS 30  // the desired frames per second
#define WIDTH 32 // width of the display

#ifdef ARDUINO_DUE
#define HEIGHT 17 // height of the display
#else
#define HEIGHT 1 // height of the display
#endif

#define GAMMA 25 // 10x the gamma value used; this cant be changed willynilly; the table must be valid for the given value
#define STATUS_OUTPUT_TIME 10000000 // how often in micros to output the BT status and effective FPS
//#define HEARTBEAT_TIMEOUT 10000000 // how long in micros to keep the connection active after the last heartbeat was received
#define MAX_TEXT_LENGTH 140 // the longest text allowed
#define MAX_TOUCHES 5 // max simultaneous touches tracked
#define IS_TOUCHING_TIMEOUT 100 // # of milliseconds before a touch expires
#define BT_SEND_SETTINGS_DELAY 3000000 // how long to wait in microseconds before sending the BT settings

#define NUM_PALETTES 201 // how many palettes exist in the palette array

#ifdef ARDUINO_DUE
#define PALETTE_SIZE 1024 // the number of colors to put in each palette
#else
#define PALETTE_SIZE 50 // the number of colors to put in each palette
#endif

#define MIN_SATURATION 0 // min saturation to use for base colors

typedef struct {
  uint8_t r[PALETTE_SIZE];
  uint8_t g[PALETTE_SIZE];
  uint8_t b[PALETTE_SIZE];
  int index;
} Palette;


#define DEBUG_LEVEL 1
#define p(...) Serial.print(__VA_ARGS__)

#define USE_BLUETOOTH 1

// These are implemented on a per-hardware basis
void set_pixel(int x, int y, int r, int g, int b);
void update_LEDs();
void Matrix_init();

// Define the command types; the structure is 1-byte command, multi-byte payload, end with 255                                                                                                                                     
enum Command {
  CMD_NONE         = 0x00, // noop; e.g. can be used to send BTLE 'credits' from iOS to the BTLE shield                                                                                                                                                      
  CMD_NEXT_PROGRAM = 0x01, // one arg (up/down)                                                                                                                                                    
  CMD_NEXT_PALETTE = 0x02, // one arg (up/down)                                                                                                                                                    
  CMD_TEXT_ENTRY   = 0x03, // text string in payload; text entry on ipad                                                                                                                           
  CMD_SET_SPEED    = 0x04, // one arg                                                                                                                                                              
  CMD_SET_COLORCYCLING    = 0x05, // one arg                                                                                                                                                       
  CMD_SET_ZOOM  = 0x06, // one arg                                                                                                                                                              
  CMD_SET_CUSTOM  = 0x07, // one arg                                                                                                                                                              
  CMD_RESET        = 0x08, // one arg (up/down)                                                                                                                                                    
  CMD_SET_BRIGHTNESS = 0x09, // one arg                                                                                                                                                            
  CMD_SET_MIN_SATURATION = 0x0A, // one arg                                                                                                                                                        
  CMD_SET_PROGRAM    = 0x0B, // one arg                                                                                                                                                            
  CMD_SET_PALETTE    = 0x0C, // one arg                                                                                                                                                            
  CMD_SET_POSITION    = 0x0D, // one arg                                                                                                                                                            
  CMD_SET_COLOR_INDEX    = 0x0E, // one arg
  CMD_SET_HUE_SHIFT    = 0x0F, // one arg

  CMD_SET_TOUCH = 0x17, // two args                                                                                                                                                               
  CMD_HEARTBEAT = 0x1C, // no args  
};
#define MAX_ARG 254 // maximum command argument value since 255 is a special value


#endif
