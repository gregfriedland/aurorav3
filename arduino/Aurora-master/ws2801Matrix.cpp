/*
Interface with WS2801 drivers using bit banging

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#include <Arduino.h>
#include "Aurora.h"
#include <stdint.h>
#include "gamma2.h"

#ifdef MATRIX_WS2801

#define DATAPIN 3
#define CLOCKPIN 2

static baseType data[WIDTH][HEIGHT]; // pack 4 bytes per word

#ifdef ARDUINO_DUE
volatile RwReg *dataport, *clockport;
uint32_t datapin, clockpin;
#else
volatile uint8_t *dataport, *clockport;
uint8_t datapin, clockpin;
#endif

// Initialize the matrix
void Matrix_init() {
#ifdef ARDUINO_DUE
    // allow sychronous PIO writes
    REG_PIOA_OWER = 0xFFFFFFFF;
    REG_PIOB_OWER = 0xFFFFFFFF;
    REG_PIOC_OWER = 0xFFFFFFFF;
    REG_PIOD_OWER = 0xFFFFFFFF;
#endif

    dataport  = portOutputRegister(digitalPinToPort(DATAPIN));
    clockport = portOutputRegister(digitalPinToPort(CLOCKPIN));    
    datapin   = digitalPinToBitMask(DATAPIN);
    clockpin  = digitalPinToBitMask(CLOCKPIN);
  
    pinMode(DATAPIN, OUTPUT);
    pinMode(CLOCKPIN, OUTPUT);
}

// Send a single RGB 24-bit triplet.
void sendRGB(int x, int y, uint32_t tick, uint32_t tock) {
  uint32_t color = data[x][y];
  // Iterate through color bits (red MSB)
  for(int b=23; b>=0; b--) {
    *clockport = tick;
    if (color & (1<<b)) *dataport |= datapin;
    else *dataport &= ~datapin;
    
    // stall about 0.25us on the Due, to give the WS2801 time to register the change.
    for (int i=0; i<20; i++) asm("nop\n");
    
    *clockport = tock;
  }
}  


// Call to refresh the LEDs. Sends all the data to the WS2801s.
void update_LEDs() {
  uint32_t tick = *clockport & ~clockpin;
  uint32_t tock = *clockport | clockpin;  
//  int dx = 1;
  for (int y=0; y<HEIGHT; y++) {
//    if (dx == 1) {
      for (int x=0; x<WIDTH; x++) {
        sendRGB(x, y, tick, tock);
      }
//    } else {
//      for (int x=WIDTH-1; x>=0; x--) {
//        sendRGB(x, y, tick, tock);
//      }      
//    }
//    dx = -dx;
  }
  *clockport = tick;
  delayMicroseconds(500);
}

// set a single pixel
void set_pixel(int x, int y, int r, int g, int b) {
  // take into account how the wall is wired
  y = HEIGHT - 1 - y;
  if (y%2 == 0) {
    x = WIDTH - 1 - x;
  }

#if DEBUG_LEVEL >= 5
  if (x==0 && y==0) {
    p("r="); p(r); p(" g="); p(g); p(" b="); p(b); p("\n");
  }
#endif
  
  // gamma correction
#if GAMMA == 25
  r = gamma25[r];
  g = gamma25[g];
  b = gamma25[b];
#endif

  uint32_t dat = b & 0xFF;
  dat |= (g & 0xFF) << 8;
  dat |= (r & 0xFF) << 16;
  data[x][y] = dat;
}

#endif

