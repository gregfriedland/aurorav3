  
/*
Main entry point for the revised Aurora LED wall written to run on an Arduino Due.

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

// TODO:

// libraries used by submodules have to be included in the main .ino file
#include <SPI.h>
#include <DueTimer.h>

#include "Aurora.h"
#include "bzr.h"
#include "Drawer.h"
#include "AlienBlob.h"
#include "OffDrawer.h"
#include "TextDrawer.h"
#include "PaintDrawer.h"
#include "PhageDrawer.h"
#include "util.h"

#if USE_BLUETOOTH
#include <ble_system.h>
#include <ble_shield_uart.h>
#endif

DrawerSettings *settings[MAX_USED_DRAWER]; // stores all the drawer settings
Drawer *drawer = NULL;  // pointer to the current drawer
Palette *palette; // the current color palette

uint32_t lastPrintTime, lastUpdateTime, interval, iter;

#if USE_BLUETOOTH
#define MAX_CMD_ARGS 10 // the max number of char arguments per command
#define BLE_BUFFER_LEN 140
void BLEConnected() { sendSettings(); } // callback for establishing a bluetooth connection
void BLEDisconnected() {} // callback for disconnecting
byte bleBuffer[BLE_BUFFER_LEN];
#endif

void setup() {
  Serial.begin(57600);

  Matrix_init();

  lastPrintTime = micros();
  lastUpdateTime = micros();
  interval = 1000000 / FPS;
  iter = 0;  
  
#ifdef ARDUINO_DUE
  randomSeed(trueRandom());
#else
  randomSeed(analogRead(0));
#endif

  palette = (Palette*) malloc(sizeof(Palette));
  int paletteIndex = random(NUM_PALETTES);
  loadPalette(paletteIndex, palette);
#if DEBUG_LEVEL >= 1
  p("Pal "); p(paletteIndex); p("\n");
#endif
  
  createDefaultDrawerSettings();
  changeDrawer(DRAWER_ALIENBLOB);
  
#if USE_BLUETOOTH
  ble_setup(BLEConnected, BLEDisconnected);
#endif
  
#ifdef ARDUINO_UNO
  Timer1.initialize();
#endif
#if DEBUG_LEVEL >= 1
  p("Setup done\n");
#endif
}
     
void loop() {
  unsigned int newTime = micros();  
  if (newTime - lastUpdateTime > interval) {
    lastUpdateTime = newTime;

    drawer->draw();
    update_LEDs();

    iter++;
    if (micros() - lastPrintTime >= STATUS_OUTPUT_TIME) { 
      int diff = micros() - lastPrintTime;
#if DEBUG_LEVEL >= 1
      p(iter * 1000000 / diff); p(" fps ");
#if USE_BLUETOOTH
      if (ble_is_connected()) p("BT\n");
      else p("noBT\n");
#endif
#endif
      lastPrintTime = micros();
      iter = 0;
    }
  }

#if USE_BLUETOOTH    
  int nBytes;
  do {
    ble_loop();

    nBytes = ble_pop_rx_data(bleBuffer, BLE_BUFFER_LEN, 0xFF);
    if (nBytes > 0) {
      processCmd(bleBuffer[0], bleBuffer+1, nBytes-1);
    }
  } while (nBytes > 0);
#endif
}        


// change the drawer and free up the old one's memory
void changeDrawer(int drawerType) {
  if (drawer != NULL) {
    delete drawer;
    drawer = NULL;
  }
    
  switch(drawerType) {
    case DRAWER_TEXT:
      drawer = new TextDrawer(WIDTH, HEIGHT, settings[DRAWER_TEXT]);
      break;
    case DRAWER_BZR:
      drawer = new Bzr(WIDTH, HEIGHT, settings[DRAWER_BZR]);
      break;
    case DRAWER_ALIENBLOB:
      drawer = new AlienBlob(WIDTH, HEIGHT, settings[DRAWER_ALIENBLOB]);
      break;
    case DRAWER_PAINT:
      drawer = new PaintDrawer(WIDTH, HEIGHT, settings[DRAWER_PAINT]);
      break;
    case DRAWER_PHAGE:
      drawer = new PhageDrawer(WIDTH, HEIGHT, settings[DRAWER_PHAGE]);
      break;
    case DRAWER_OFF:
      drawer = new OffDrawer(WIDTH, HEIGHT, settings[DRAWER_OFF]);
      break;
  }
  
  drawer->setup();
#if USE_BLUETOOTH
  sendSettings();
#endif
  
  drawer->setAll(0,0,0);
  update_LEDs();
}

// Initialize starting drawer settings that look good from the start
void createDefaultDrawerSettings() {
  settings[DRAWER_TEXT] = new DrawerSettings(1,1,1,1, palette, 1); strcpy(settings[DRAWER_TEXT]->text,"Hello there!");
  settings[DRAWER_BZR] = new DrawerSettings(0.1, 0.1, 0.63, 1, palette, BZR_SPEED_MULTIPLIER); 
  settings[DRAWER_ALIENBLOB] = new DrawerSettings(0.2, 0.1, 0.1, 1, palette, ALIENBLOB_SPEED_MULTIPLIER);
  settings[DRAWER_PAINT] = new DrawerSettings(0,0,0,0, palette, 1);
  settings[DRAWER_PHAGE] = new DrawerSettings(0.5,0,0,0, palette, PHAGE_SPEED_MULTIPLIER);
  settings[DRAWER_OFF] = new DrawerSettings(0,0,0,0, palette, 1);
}


#if USE_BLUETOOTH
// callback to process a single command given data in bytes
void processCmd(byte cmd, byte *data, int nbytes) {
  float val, val2;
  int iVal;

  int numArgs = nbytes;
  
#if DEBUG_LEVEL >= 2
  p("Processing cmd "); p(cmd); p(" "); p(nbytes); p(": ");
  for (int i=0; i<numArgs; i++ ) {
    p(" "); p(data[i], DEC);
  }
  p("\n");
#endif
  
  if (cmd == CMD_TEXT_ENTRY) {
    if (nbytes < 2) return;
    nbytes = MIN(nbytes, MAX_TEXT_LENGTH-1);
    data[nbytes] = 0; // replace 255 with string terminator
//#if DEBUG_LEVEL > 1
    //p("Text entry ("); p(nbytes); p(" bytes): "); p((char*)data); p("\n");
//#endif

    char *text = drawer->getSettings()->text;
    for (int i=0; i<nbytes; i++) {
      text[i] = (char) data[i];
    }
    text[nbytes] = 0;
    return;
  }
  
  if (nbytes >= MAX_CMD_ARGS) {
#if DEBUG_LEVEL >= 1
    p("Rcvd too many args for cmd "); p(cmd); p(": "); p(nbytes); p("\n"); 
#endif
    return;
  }

  // perform the command specific actions
  switch(cmd) {
  case CMD_NONE:
#if DEBUG_LEVEL>=1
    p("Noop\n");
#endif
    break;

//  case CMD_HEARTBEAT:
//#if DEBUG_LEVEL>=2      
//    p("Heartbeat\n");
//#endif
//    lastHeartbeatTime = micros();
//    break;

  case CMD_NEXT_PROGRAM:
#if DEBUG_LEVEL>=1      
    p("Next program\n");
#endif
    iVal = (drawer->getType()+1) % MAX_USED_DRAWER;
    changeDrawer(iVal);
    break;

  case CMD_RESET:
#if DEBUG_LEVEL>=1      
    p("Reset\n");
#endif
    drawer->reset();
    break;

  case CMD_SET_SPEED:
    if (numArgs < 1) break;
      //val = data[0] / float(MAX_ARG) * 2 - 1 ;
      val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1      
    p("Set speed: "); p(val); p("\n");
#endif
    drawer->getSettings()->speed = val;
    break;

  case CMD_SET_COLORCYCLING:
    if (numArgs < 1) break;
      //val = data[0] / float(MAX_ARG) * 2 - 1;
      val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1      
    p("Set color cycling: "); p(val); p("\n");
#endif
    drawer->getSettings()->colorCycling = val;
    break;

  case CMD_SET_ZOOM:
    if (numArgs < 1) break;
    val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1      
    p("Zoom: "); p(val); p("\n");
#endif
    drawer->getSettings()->zoom = val;
    break;

  case CMD_SET_CUSTOM:
    if (numArgs < 1) break;
    val = data[0] / float(MAX_ARG);
#if DEBUG_LEVEL>=1      
    p("Set custom: "); p(val); p("\n");
#endif
    drawer->getSettings()->custom = val;
    break;

  case CMD_SET_MIN_SATURATION:
    if (numArgs < 1) break;
    iVal = data[0];
#if DEBUG_LEVEL>=1
    p("Set min saturation: "); p(iVal); p("\n");
#endif
    drawer->getSettings()->minSaturation = iVal;
    break;
      
  case CMD_SET_HUE_SHIFT:
    if (numArgs < 1) break;
    iVal = data[0];
#if DEBUG_LEVEL>=1
    p("Set hue shift: "); p(iVal); p("\n");
#endif
    drawer->getSettings()->hueShift = iVal;  // hue actually goes up to 360
    break;
      
  case CMD_SET_PROGRAM:
    if (numArgs < 1) break;
    iVal = data[0];
    if (iVal >= MAX_USED_DRAWER) break;
#if DEBUG_LEVEL>=1      
    p("Set program: "); p(iVal); p("\n");
#endif
    iVal = iVal % MAX_USED_DRAWER;
    changeDrawer(iVal);
    break;

  case CMD_SET_PALETTE:
    if (numArgs < 1) break;
    iVal = data[0];
    if (iVal >= NUM_PALETTES) break;
#if DEBUG_LEVEL>=1      
    p("Set palette: "); p(iVal); p("\n");
#endif
    loadPalette(iVal, drawer->getSettings()->palette);
    break;

  case CMD_SET_POSITION:
    if (numArgs < 2) break;
    iVal = (data[0] * 255L + data[1]) - 255*127L; // int between -255*127 -> 255*127
#if DEBUG_LEVEL>=1
    p("Set position: "); p(iVal); p("\n");
#endif
    drawer->getSettings()->pos = iVal;
    break;
  
  case CMD_SET_COLOR_INDEX:
    if (numArgs < 2) break;
    iVal = (data[0] * MAX_ARG + data[1]) - MAX_ARG*127L; // int between -254*127 -> 254*127
#if DEBUG_LEVEL>=1
    p("Set color index: "); p(iVal); p("\n");
#endif
    drawer->getSettings()->colorIndex = iVal;
    break;
  
  case CMD_SET_TOUCH:
    if (numArgs < 3) break;
    iVal = data[0];
    if (iVal >= MAX_TOUCHES) break;
    val = round(data[1] / float(MAX_ARG) * WIDTH);
    val2 = round(data[2] / float(MAX_ARG) * HEIGHT);
#if DEBUG_LEVEL>=1      
    p("Touch"); p(iVal); p(": "); p(data[1]); p(" "); p(data[2]); p("\n");
#endif
    drawer->getSettings()->setTouch(iVal, val, val2);
    break;

  default:
#if DEBUG_LEVEL>=1      
    p("Unknown command "); p(cmd); p(": ");
    for (int i=0; i<numArgs; i++) {
      p(data[i], DEC); p(" ");
    }
    p("\n");
#endif    
    break;
  }
}

// send the current settings values back to the controller
void sendSettings() {
  if (!ble_is_connected()) return;
  
  //byte buf[3] = {CMD_SET_SPEED, int((drawer->getSettings()->speed + 1)/2.0  * MAX_ARG), 255};
  byte buf[3] = {CMD_SET_SPEED, int(drawer->getSettings()->speed * MAX_ARG), 255};
  ble_tx(buf, 3);

  //byte buf2[3] = {CMD_SET_COLORCYCLING, int((drawer->getSettings()->colorCycling + 1)/2.0 * MAX_ARG), 255};
  byte buf2[3] = {CMD_SET_COLORCYCLING, int(drawer->getSettings()->colorCycling * MAX_ARG), 255};
  ble_tx(buf2, 3);

  byte buf4[3] = {CMD_SET_PALETTE, int(drawer->getSettings()->palette->index * MAX_ARG / NUM_PALETTES), 255};
  ble_tx(buf4, 3);

  byte buf5[4] = {CMD_SET_PROGRAM, drawer->getType(), 0xFF};
  ble_tx(buf5, 3);

#if DEBUG_LEVEL >= 1
  p("Done sending settings\n");
#endif
}

#endif // USE_BLUETOOTH



