/*
The AlienBlob Drawer: waving/oscillating patterns constructed with Perlin noise

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef ALIENBLOB_H
#define ALIENBLOB_H

#include "Drawer.h"
#include "Perlin.h"
#include <math.h>
#include "util.h"

#define ALIENBLOB_SPEED_MULTIPLIER 7

class AlienBlob : public Drawer {
 public:
 AlienBlob(uint16_t width, uint16_t height, DrawerSettings *settings) :
  Drawer(width, height, settings) {
    // precalculate 1 period of the sine wave (360 degrees)
    for (int i=0; i<360; i++) sineTable[i] = sin(2*3.14159*i/360);
  }

  int getType() { return DRAWER_ALIENBLOB; }

  void setup() {    
    incr = 0.03125;
    xoffIncr = 0.003; //0.3;
    yoffIncr = 0.0007; //0.07;
    noiseMult = 10; //3
    
    settings->pos = random(1<<8);
  }

  void reset() { settings->pos = random(1<<8); }

 void draw() {
    Drawer::draw();

    float multiplier = settings->zoom * 10;
    //float zoffIncr2 = zoffIncr * settings->speed;
    float incr2 = incr*multiplier;

    float h;
    float xx;
    float yy = 0; 
 
    for (int y=0; y<height; y++) {
      xx = 0; 
      for (int x=0; x<width; x++) {
        //float nf = pnoise(xx, yy, zoff);
        int ni = pnoise_fp(xx * (1<<16), yy * (1<<16),  (settings->pos/100.0) * (1<<16));
        float nif = float(ni) / (1<<16);
        
        float n = nif; // float(n) / (1<<16);
    
        // use pre-calculated sine results
        int deg = int(360/2/PI * (n*noiseMult + 4*PI)) % 360;
        h = sineTable[deg] / 2 + 0.5;
        int index = int(h*(getNumColors()-1));
#if DEBUG_LEVEL >= 3
        if (x==0 && y==0) {
          p("Alien n="); p(n); p(" sin("); p(deg); p(") -> "); p(index); p("\n");
        }
#endif        
	             
        // determine pixel color
        setColor(x,y,index);
         
        xx += incr2;
      }
       
      yy += incr2;
    }
     
    // move through noise space -> animation
    //zoff += zoffIncr2;
  }
  
 private:
  float zoff;
  float incr, xoffIncr, yoffIncr, noiseMult;
  float sineTable[360];
};

#endif
