//
//  platform.h
//  ArduinoV3
//
//  Created by Greg Friedland on 7/20/13.
//  Copyright (c) 2013 Greg Friedland. All rights reserved.
//

#ifndef _platform_h
#define _platform_h


typedef void (*CallbackFunc)(void);

void scheduleCallback(CallbackFunc callback, int microsDelay);

void startRNG();

#endif
