/*
The BZ reaction drawer: display organic oscillating complex patterns

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef BZR_H
#define BZR_H

#include <stdint.h>
#include "Drawer.h"
#include "Aurora.h"
#include <Arduino.h>

// macro to access one of the arrays
#define A(arr, x, y, z) (arr)[(z) + NZ*(y) + NZ*NY*(x)]

#define WIDTH2 48
#define HEIGHT2 24
#define NX WIDTH2
#define NY HEIGHT2
#define NZ 2
#define FIXED_POINT_SCALE (1<<15)
#define PRESS_RADIUS 1

#define BZR_SPEED_MULTIPLIER 20 // not used
//#define BZR_NUM_STATES 20

typedef int bzrType;

class Bzr : public Drawer {
 public:
  Bzr(int width, int height, DrawerSettings *settings) : 
    Drawer(width, height, settings) {
#if DEBUG_LEVEL>=1
    p("Bzr init: using "); p(NX*NY*NZ*sizeof(float)*3); p(" bytes RAM\n");
#endif    
    state = 0;
    
    // prevent expensive (?) modulo ops in wraparound check
    for (int x=0; x<NX; x++) {
      prevX[x] = (x-1+NX)%NX;
      nextX[x] = (x+1+NX)%NX;
    }
    for (int y=0; y<NY; y++) {
      prevY[y] = (y-1+NY)%NY;
      nextY[y] = (y+1+NY)%NY;
    }    
  }
  
  int getType() { return DRAWER_BZR; }
  void reset() { randomDots(0, 0, WIDTH2, HEIGHT2); }
  
  void randomDots(int minx, int miny, int maxx, int maxy) {
    for (int x=minx; x<maxx; x++) {
      for (int y=miny; y<maxy; y++) {
        //print("randomDots: %d %d\n", x, y);
        A(a,x,y,p) = random(FIXED_POINT_SCALE);
        A(b,x,y,p) = random(FIXED_POINT_SCALE);
        A(c,x,y,p) = random(FIXED_POINT_SCALE);
      }
    }
  }

  void highDots(int minx, int miny, int maxx, int maxy) {
    for (int y=miny; y<maxy; y++) {
      for (int x=minx; x<maxx; x++) {
        A(a,x,y,p) = 1;
        A(b,x,y,p) = 1;
        A(c,x,y,p) = 1;
      }
    }
  }    

  void draw() {
    // Don't use 'pos' because we can't go backwards
    Drawer::draw();
    //float speed = (settings->speed + 1) / 2;
    float speed = settings->speed;
    
    int numStates = 20 - speed * 19;
    if (state >= numStates) state = numStates-1;
  
    if (state == 0) {
      for (int x=0; x<WIDTH2; x++) {
        for (int y=0; y<HEIGHT2; y++) {
          bzrType c_a = A(a, prevX[x], prevY[y], p) + 
                A(a, prevX[x], y       , p) + 
                A(a, prevX[x], nextY[y], p) + 
                A(a, x       , prevY[y], p) + 
                A(a, x       , y       , p) + 
                A(a, x       , nextY[y], p) + 
                A(a, nextX[x], prevY[y], p) + 
                A(a, nextX[x], y       , p) + 
                A(a, nextX[x], nextY[y], p);
                
          bzrType c_b = A(b, prevX[x], prevY[y], p) + 
                A(b, prevX[x], y       , p) + 
                A(b, prevX[x], nextY[y], p) + 
                A(b, x       , prevY[y], p) + 
                A(b, x       , y       , p) + 
                A(b, x       , nextY[y], p) + 
                A(b, nextX[x], prevY[y], p) + 
                A(b, nextX[x], y       , p) + 
                A(b, nextX[x], nextY[y], p);
  
          bzrType c_c = A(c, prevX[x], prevY[y], p) + 
                A(c, prevX[x], y       , p) + 
                A(c, prevX[x], nextY[y], p) + 
                A(c, x       , prevY[y], p) + 
                A(c, x       , y       , p) + 
                A(c, x       , nextY[y], p) + 
                A(c, nextX[x], prevY[y], p) + 
                A(c, nextX[x], y       , p) + 
                A(c, nextX[x], nextY[y], p);
  
          c_a = c_a / 89 * 10;
          c_b = c_b / 89 * 10;
          c_c = c_c / 89 * 10;
    
          A(a,x,y,q) = constraini( c_a + c_a * ( c_b - c_c ) / FIXED_POINT_SCALE, 0, FIXED_POINT_SCALE);
          A(b,x,y,q) = constraini( c_b + c_b * ( c_c - c_a ) / FIXED_POINT_SCALE, 0, FIXED_POINT_SCALE);
          A(c,x,y,q) = constraini( c_c + c_c * ( c_a - c_b ) / FIXED_POINT_SCALE, 0, FIXED_POINT_SCALE);
          //if (x==0 && y==0) printf("c_a=%d c_b=%d c_c=%d a(0,0,q)=%d\n", c_a/(FIXED_POINT_SCALE/1000), c_b/(FIXED_POINT_SCALE/1000), c_c/(FIXED_POINT_SCALE/1000), A(a,x,y,q)/(FIXED_POINT_SCALE/1000));
        }
      }
    
      if (p == 0) {
        p = 1;
        q = 0;
      } else {
        p = 0;
        q = 1;
      }
    }

   // the zoom should range from (0.1 to 1) * factor
   // to use no zoom, set settings->zoom = (32 / 48 - 0.1) / 0.9 ~= 0.63
   float zoom = (settings->zoom * 0.9 + 0.1) * WIDTH2 / width;
   for (int x=0; x<width; x++) {
     for (int y=0; y<height; y++) {
       int x2 = int(x * zoom);
       int y2 = int(y * zoom);

       bzrType a2;
       a2 = interp32(A(a,x2,y2,q), A(a,x2,y2,p), state, numStates);

       int index = float(a2) / FIXED_POINT_SCALE * (getNumColors()-1); // * settings->custom;
       setColor(x, y, index);
     }
   }

   for (int i=0; i<MAX_TOUCHES; i++) {
      int x, y;
      if (settings->isTouching(i, &x, &y)) {
        highDots(constraini(x-PRESS_RADIUS, 0, width), constraini(y-PRESS_RADIUS, 0, height), constraini(x+PRESS_RADIUS, 0, width), constraini(y+PRESS_RADIUS, 0, height));
      }
    }
   
   state++;
   if (state >= numStates) state = 0;    
  }

  void setup() {
    p = 0;
    q = 1;
  
    randomDots(0, 0, WIDTH2, HEIGHT2);
  }  

 private:  

  int constraini(int val, int low, int hi) {
    if (val < low) return low;
    else if (val > hi) return hi;
    else return val;
  }
 
  int interp32(int start, int end, int index, int length) {
    return end*index/length + start*(length-index)/length;
  }

  int p, q, state;
  bzrType a[NX*NY*NZ], b[NX*NY*NZ], c[NX*NY*NZ];
  int prevX[NX], nextX[NX], prevY[NY], nextY[NY];
};

#endif
