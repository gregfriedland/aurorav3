
/*
Code for interacting with the Adafruit RGB matrix: http://www.adafruit.com/products/420

Uses an approach called Bit Angle Modulation (http://www.batsocks.co.uk/readme/art_bcm_3.htm) as opposed to PWM.
Heavily influenced by https://github.com/adafruit/RGB-matrix-Panel/blob/master/RGBmatrixPanel.cpp 
and http://www.rayslogic.com/propeller/programming/AdafruitRGB/AdafruitRGB.htm

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#if MATRIX_ADAFRUIT

#include <Arduino.h>
#include "Aurora.h"
#include <stdint.h>
#include "gamma2.h"
#include "util.h"

#define COLOR_DEPTH 8
#define NUM_COLORS (1<<COLOR_DEPTH)
#define TICK_US 7

// must all be on the same port; if these are changed, they byte shifting in set_pixel must be changed as well
#define R1_PIN 10 // Due C29 
#define G1_PIN 3  // Due C28 
#define B1_PIN 7  // Due C23
#define R2_PIN 6  // Due C24
#define G2_PIN 5  // Due C25
#define B2_PIN 4  // Due C26

#define CLK_PIN 13 // Due B27 - Orange wire
#define OE_PIN 12  // Due D8 - Long green wire
#define A_PIN A0   // Due A0 78 A16 - White wire
#define B_PIN A1   // Due A1 79 A24 - White wire
#define C_PIN A2   // Due A2 80 A23 - White wire
#define LAT_PIN 11 // Due D7 - Yellow wire

#define DATAPORT_MASK (B1101111 << 23)  // only the pins we are using on the data port

void update_leds_one_section_opt();

#ifdef ARDUINO_DUE
// Timer interrupt for updating the display
void TC3_Handler() {
  TC_GetStatus(TC1, 0);
  update_leds_one_section_opt();
}
volatile RwReg *latport, *oeport, *addraport, *addrbport, *addrcport, *sclkport, *datport;
#else
volatile uint8_t *latport, *oeport, *addraport, *addrbport, *addrcport, *sclkport, *datport;
#endif


volatile uint32_t bitlevel = 0;
static volatile uint8_t section=0, buf_num=0, swap_buf=false;
static uint32_t data[2][8][COLOR_DEPTH][WIDTH/4]; // pack 4 bytes per word


uint32_t sclkpin, latpin, oepin, addrapin, addrbpin, addrcpin, addrdpin;

// Call this to start using the matrix.
// Initialize the port and pin variables and open the pins as output. Start the display interrupt timer.
void Matrix_init() {
#ifdef ARDUINO_DUE
    // allow sychronous PIO writes
    REG_PIOA_OWER = 0xFFFFFFFF;
    REG_PIOB_OWER = 0xFFFFFFFF;
    REG_PIOC_OWER = 0xFFFFFFFF;
    REG_PIOD_OWER = 0xFFFFFFFF;
#endif

    datport  = portOutputRegister(digitalPinToPort(R1_PIN));
    sclkport  = portOutputRegister(digitalPinToPort(CLK_PIN));
    sclkpin   = digitalPinToBitMask(CLK_PIN);
    latport   = portOutputRegister(digitalPinToPort(LAT_PIN));
    latpin    = digitalPinToBitMask(LAT_PIN);
    oeport    = portOutputRegister(digitalPinToPort(OE_PIN));
    oepin     = digitalPinToBitMask(OE_PIN);
    addraport = portOutputRegister(digitalPinToPort(A_PIN));
    addrapin  = digitalPinToBitMask(A_PIN);
    addrbport = portOutputRegister(digitalPinToPort(B_PIN));
    addrbpin  = digitalPinToBitMask(B_PIN);
    addrcport = portOutputRegister(digitalPinToPort(C_PIN));
    addrcpin  = digitalPinToBitMask(C_PIN);
  
    pinMode(R1_PIN, OUTPUT);
    pinMode(G1_PIN, OUTPUT);
    pinMode(B1_PIN, OUTPUT);
    pinMode(R2_PIN, OUTPUT);
    pinMode(G2_PIN, OUTPUT);
    pinMode(B2_PIN, OUTPUT);
    *datport = 0;
  
    pinMode(CLK_PIN, OUTPUT); *sclkport   &= ~sclkpin;  // Low 
    pinMode(LAT_PIN, OUTPUT); *latport   &= ~latpin;   // Low 
    pinMode(OE_PIN, OUTPUT); *oeport  &= ~oepin;     // High (disable output)                    
    pinMode(A_PIN, OUTPUT); *addraport &= ~addrapin; // Low
    pinMode(B_PIN, OUTPUT); *addrbport &= ~addrbpin; // Low
    pinMode(C_PIN, OUTPUT); *addrcport &= ~addrcpin; // Low

    Serial.print("Running at ");
    Serial.print(1000000.0 / TICK_US / (NUM_COLORS-1) / 8);
    Serial.println(" fps");
    p("RGB matrix using "); p(2*8*COLOR_DEPTH*WIDTH/4*4); p(" bytes RAM\n");

#ifdef ARDUINO_DUE
    startTimer(TC1, 0, TC3_IRQn, 1000000/TICK_US);
#endif
}

// Swap the buffers for double buffering
void swap_buffers(bool copy) {
  // To avoid 'tearing' display, actual swap takes place in the interrupt                                                                                                                        
  // handler, at the end of a complete screen refresh cycle.          
  swap_buf = true;                  // Set flag here, then...                                                                                                                                    
  while(swap_buf == true) delayMicroseconds(1); // wait for interrupt to clear it                                                                                                                            
  if (copy == true)
    memcpy(data[buf_num], data[1-buf_num], WIDTH / 4 * 8 * COLOR_DEPTH);
}

// Call this to update the LED matrix. Really just swaps the buffers.
void update_LEDs() {
  swap_buffers(true);
}

// set a single pixel and store the bit mask for each color level so we can retrieve it quickly
void set_pixel(int x, int y, int r, int g, int b) {
  if (DEBUG_LEVEL >= 4 && x==0 && y==0) {
    p("r="); p(r); p(" g="); p(g); p(" b="); p(b); p("\n");
  }
  
  // gamma correction
#if GAMMA == 25
  r = gamma25[r];
  g = gamma25[g];
  b = gamma25[b];
#endif

  uint8_t sect = y%8;

  for (int depth=0; depth<COLOR_DEPTH; depth++) {
    uint32_t dat = data[buf_num][sect][depth][x/4];
    int byte_shift = 8 * (x%4);
    
    // pack the data into a byte where it can be quickly extracted
    // these shifts are determined by the ARM pin #
    if (y < 8) {
      if (r & (1<<depth)) dat |= 1<<6<<byte_shift; else dat &= ~(1<<6<<byte_shift);
      if (g & (1<<depth)) dat |= 1<<5<<byte_shift; else dat &= ~(1<<5<<byte_shift);
      if (b & (1<<depth)) dat |= 1<<0<<byte_shift; else dat &= ~(1<<0<<byte_shift);
    } else {
      if (r & (1<<depth)) dat |= 1<<1<<byte_shift; else dat &= ~(1<<1<<byte_shift);
      if (g & (1<<depth)) dat |= 1<<2<<byte_shift; else dat &= ~(1<<2<<byte_shift);
      if (b & (1<<depth)) dat |= 1<<3<<byte_shift; else dat &= ~(1<<3<<byte_shift);
    }
    
    data[buf_num][sect][depth][x/4] = dat;
  }
}

// output data in bytes for each row in fourths: GO1 GO2 GO3 GO4
#define SET_DATAPORT(dataByte) { *datport = (*datport & ~DATAPORT_MASK) | ((dataByte) & DATAPORT_MASK); }
#define GO1(ptr, tick, tock) { SET_DATAPORT((*(ptr)) << 23);  *sclkport = (tick); asm("nop\n"); *sclkport = (tock); }
#define GO2(ptr, tick, tock) { SET_DATAPORT((*(ptr)) << 15);  *sclkport = (tick); asm("nop\n"); *sclkport = (tock); }
#define GO3(ptr, tick, tock) { SET_DATAPORT((*(ptr)) << 7);   *sclkport = (tick); asm("nop\n"); *sclkport = (tock); }
#define GO4(ptr, tick, tock) { SET_DATAPORT((*(ptr)) >> 1); *sclkport = (tick); asm("nop\n"); *sclkport = (tock); ptr++;}


void update_leds_one_section_opt() {
#ifdef ARDUINO_DUE  
  int new_freq = 1000000 / TICK_US / (1<<bitlevel);
  startTimer(TC1, 0, TC3_IRQn, new_freq);  
#endif

  uint32_t *ptr = &data[1-buf_num][section][bitlevel][0];
  uint32_t tick = *sclkport | sclkpin;
  uint32_t tock = *sclkport & ~sclkpin;
  
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 
  GO1(ptr, tick, tock); GO2(ptr, tick, tock); GO3(ptr, tick, tock); GO4(ptr, tick, tock); 

  // disable display while latching and selecting this section
  *oeport |= oepin;
  
  // latch the data
  *latport |= latpin; 
  
  if (section & 0x01) *addraport |= addrapin; else *addraport &= ~addrapin;
  if (section & 0x02) *addrbport |= addrbpin; else *addrbport &= ~addrbpin;
  if (section & 0x04) *addrcport |= addrcpin; else *addrcport &= ~addrcpin;

  // enable the display
  *oeport  &= ~oepin;  
  *latport &= ~latpin;
  
  bitlevel++;
  if (bitlevel >= COLOR_DEPTH) {
    bitlevel = 0;
  
    section++;
    if (section >= 8) {
      section = 0;

      if(swap_buf == true) {    // Swap front/back buffers if requested                                                                                                                            
        buf_num = 1 - buf_num;
        swap_buf = false;
      }
    }
  }
  //counter++;
}

#endif

