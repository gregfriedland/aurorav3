/*
Utility functions
*/

#ifndef UTIL_H
#define UTIL_H

#include "Aurora.h"
#include <stdint.h>
#include <Arduino.h>

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#ifdef ARDUINO_DUE
uint32_t trueRandom();
void startTimer(Tc *tc, uint32_t channel, IRQn_Type irq, uint32_t frequency);
#endif

float dist(float x1, float y1, float x2, float y2);

uint16_t interp(uint8_t start, uint8_t end, uint8_t index, uint8_t length);

#endif
