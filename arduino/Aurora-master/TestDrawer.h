/*
The Test drawer: drawer for testing the display; shows a simple moving color gradient of the palette

Copyright (C) 2013 Greg Friedland
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/

#ifndef TESTDRAWER_H
#define TESTDRAWER_H

#include <stdint.h>
#include "Drawer.h"

class TestDrawer : public Drawer {
 public:
  TestDrawer(uint16_t width, uint16_t height, DrawerSettings *settings) : 
    Drawer(width, height, settings) {
  }

  int getType() { return DRAWER_TEST; }

  void setup() {
    i = 0;
    di = 1;
  }

  void reset() {
  }
  
  void draw() {
    Drawer::draw();
    
    for (int y=0; y<height; y++) {
      for (int x=0; x<width; x++) {
        int index = x * (getNumColors()-1) / width + frameNum;
        setColor(x, y, index);
      }
    }
  }
  
 private:
  int i, di;
};

#endif
